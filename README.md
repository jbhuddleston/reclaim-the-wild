# reclaim-the-wild

Foundry Virtual TableTop game system for the Reclaim the Wild TTRPG.

The official Reclaim the Wild website is here: https://reclaimthewild.net/index.php/about/

The mirror of the ruleset is located here: https://reclaimthewild.github.io/en/

To join the discussion or monitor progress on Discord, join here; https://discord.gg/d8ujbNG 
Feel free to ask for guidance or assistance. Thank you for your interest.

If you would like to support me, you may do so here; https://ko-fi.com/jbhuddleston Thank you.
