let label = "", formula = "", value = 0;
let d = new Dialog({
    title: `Declare Action`,
    buttons: {
        assess: {
            label: `New Round`,
            callback: () => {
                label = "Assessment"
                value = 90
                d.render(true)
            }
        },
        wait: {
            label: `Wait - Ready`,
            callback: () => {
                label = "Wait - Ready"
                formula = "71"
                value = 71
                d.render(true)
            }
        },
        move: {
            label: `Move`,
            callback: () => {
                label = "Move"
                formula = "61"
                value = 61
                d.render(true)
            }
        },
        prepared: {
            label: `Cast Prepared Spell`,
            callback: () => {
                label = "Cast Prepared Spell"
                formula = "51"
                value = 51
                d.render(true)
            }
        },
        instant: {
            label: `Cast Instant Spell`,
            callback: () => {
                label = "Cast Instant Spell"
                formula = "51"
                value = 51
                d.render(true)
            }
        },
        ranged: {
            label: `Aimed Ranged Attack`,
            callback: () => {
                label = "Aimed Ranged Attack"
                formula = "41"
                value = 41
                d.render(true)
            }
        },
        thrown: {
            label: `Throw Ready Weapon`,
            callback: () => {
                label = "Throw Ready Weapon"
                formula = "41"
                value = 41
                d.render(true)
            }
        },
        longestplus: {
            label: `Longest Melee plus`,
            callback: () => {
                label = "Longest Melee plus"
                formula = "36"
                value = 36
                d.render(true)
            }
        },
        longest: {
            label: `Longest Melee`,
            callback: () => {
                label = "Longest Melee"
                formula = "35"
                value = 35
                d.render(true)
            }
        },
        long: {
            label: `Long Melee`,
            callback: () => {
                label = "Long Melee"
                formula = "34"
                value = 34
                d.render(true)
            }
        },
        short: {
            label: `Short Melee`,
            callback: () => {
                label = "Short Melee"
                formula = "33"
                value = 33
                d.render(true)
            }
        },
        hand: {
            label: `Hand Melee`,
            callback: () => {
                label = "Hand Melee"
                formula = "32"
                value = 32
                d.render(true)
            }
        },
        handminus: {
            label: `Hand Melee minus`,
            callback: () => {
                label = "Hand Melee minus"
                formula = "31"
                value = 31
                d.render(true)
            }
        },
        unaimed: {
            label: `Unaimed Ranged Attack`,
            callback: () => {
                label = "Unaimed Ranged Attack"
                formula = "21"
                value = 21
                d.render(true)
            }
        },
        unprepared: {
            label: `Cast Unprepared Spell`,
            callback: () => {
                label = "Cast Unprepared Spell"
                formula = "11"
                value = 11
                d.render(true)
            }
        },
        other: {
            label: `Other Action`,
            callback: () => {
                label = "Other Action"
                formula = "1"
                value = 1
                d.render(true)
            }
        },
        done: {
            label: `Actions Completed`,
            callback: () => {
                label = "Actions Completed"
                formula = "0"
                value = null
                d.render(true)
            }
        }
    },
    close: async html => {
        let tokens = canvas.tokens.controlled;
        // get the active Combat encounter
        let cbt = CombatEncounters.instance.active;
        if (!cbt) {
            console.error(`There was no active combat`);
            ui.notifications.error("There is no active combat");
            return;
        }
        if (value == 90) {
            // set all phase actors to defeated
            let updates = cbt.combatants.filter(c => (c.initiative && c.initiative % 10 == 0)).map(c => {
                return {
                    _id: c.id,
                    defeated: true
                }
            });
            if (updates.length) await cbt.updateEmbeddedDocuments("Combatant", updates);

            // reset all active combatants
            updates = cbt.combatants.filter(c => !c.data.defeated).map(c => {
                return {
                    _id: c.data._id,
                    initiative: null
                }
            });
            if (updates.length) await cbt.updateEmbeddedDocuments("Combatant", updates);

            // if we are using phase actors, un-defeat them to start the round
            let phase = cbt.turns[0];
            if (phase.initiative == 90) {
                await phase.update({defeated: false});
                phase = cbt.turns[1];
                await phase.update({defeated: false});
            }
            await cbt.nextRound();
            return;
        }
        for (let token of tokens) {
            let declaration = token.actor.system.dynamic["declared-action"]
            if (declaration) {
                let updates = {
                    _id: declaration._id,
                    data: {
                        label: label,
                        formula: formula,
                        value: value
                    }
                }
                await token.actor.updateEmbeddedDocuments("Item", [updates]);
                console.debug(`Declared Action [${label}] for Token [${token.name}]`);
                // get the combatant ID of the current token
                let tokenCombatant = await cbt.getCombatantByToken(token.id)
                if (!tokenCombatant) {
                    console.error(`Token not found in the combat`);
                    ui.notifications.error("Token not found in the combat");
                    return;
                }
                // update this combatant with the new initiative value
                await cbt.setInitiative(tokenCombatant.id, value);
            } else {
                console.debug(`Declared Action [${label}] for Token [${token.name}] failed`);
            }
        }
        if (tokens.length == 0) {
            console.error(`There were no tokens selected`);
            ui.notifications.error("Please select a token first");
        }

    }
},
    {
        width: 200,
        classes: ["mydialog"],
        top: 0,
        left: 0
    });
d.render(true);