main()

async function main() {
    const folderName = "Conditions";

    const folderId = game.collections.get("Folder").getName(folderName).id;
    console.log("Folder ID: ", folderId);
    const items = game.collections.get("Item").filter(function (item) {
        return item.folder?.id == folderId;
    });
    console.log("Folder Contents: ", items);

    const buttons = {};
    for (const button of Object.entries(items)) {
        buttons[button[0]] = {
            label: button[1].name,
            callback: () => {
                label = button[1].name,
                    icon = button[1].img,
                    d.render(true)
            }
        }
    }

    let d = new Dialog({
        title: `Set Status Effect`,
        buttons: buttons,
        close: async html => {
            if (icon) {
                const tokens = canvas.tokens.controlled;
                for (const token of tokens) {
                    await token.toggleEffect({
                        icon: icon,
                        id: label,
                        label: label
                    });
                    const actor = token.document.actor;
                    const inEffect = actor.system.effects.find(i => i.data.label == label) != undefined;
                    const modname = actor.slugify(label);
                    const mod = actor.system.modifiers[modname];
                    if (mod) {
                        await actor.updateEmbeddedDocuments("Item", [{ _id: mod._id, "data.inEffect": inEffect }]);
                    }
                }
                label = "", icon = "";
            }
        }
    },
        {
            width: 200,
            classes: ["mydialog"],
            top: 0,
            left: 0
        });
    d.render(true);
}