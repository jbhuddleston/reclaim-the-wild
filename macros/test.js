main()
async function main() {
    const actorImage = "icons/svg/mystery-man.svg";

    const actors = game.actors;
    console.log("Folder Contents: ", actors);

    actors.forEach(async function (actor) {
        await actor.update({"img" : actorImage, "token.texture.src": actorImage});
        console.log("Actor", actor.name);
    });
}