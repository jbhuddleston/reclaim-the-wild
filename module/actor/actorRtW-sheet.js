import { rtw } from "../config.js";
import { baseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {baseActorSheet}
 */
export class ActorRtWSheet extends baseActorSheet {

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["reclaim-the-wild", "sheet", "actor"],
      template: "systems/reclaim-the-wild/templates/actor/actorRtW-sheet.hbs",
      width: 650,
      height: 800,
      tabs: [{ navSelector: ".sheet-nav", contentSelector: ".sheet-body", initial: "main" }]
    });
  }

  /** @override */
  async getData(options) {
    if (CONFIG.rtw.testMode) console.debug("entering getData() in ActorRtW-sheet");

    const context = await super.getData(options);
    const actor = this.actor;
    const actordata = actor.system;
    // My shortcuts
    const myData = context.system;
    const dynamic = myData.dynamic;
    const tracked = myData.tracked;

    context.enrichment = {
      system: {
        biography: await TextEditor.enrichHTML(actordata.biography),
      }
    };

    // prepare the traits for display
    myData.power = []; myData.wisdom = []; myData.courage = [];
    for (let id of Object.keys(actordata.traits)) {
      const trait = actordata.dynamic[id];
      switch (trait.aspect) {
        case "power": myData.power.push(trait); break;
        case "wisdom": myData.wisdom.push(trait); break;
        case "courage": myData.courage.push(trait); break;
      }
    }

    // group and sort skills correctly for display
    let tempskills = [...myData.skills];
    let tempspells = [...myData.spells];
    myData.skills = [];
    myData.techniques = [];
    for (let item of tempskills) {
      if (item.system.category == "technique") {
        myData.techniques.push(item);
      } else {
        item.system.stepvalue = item.system.moddedvalue - item.system.value;
        myData.skills.push(item);
      }
    }
    myData.rms = [];
    for (let item of tempspells) {
      if (item.system.category == "rms") {
        myData.rms.push(item);
      } else {
        myData.skills.push(item);
      }
    }
    for (let item of myData.checks) {
      myData.skills.push(item);
    }
    delete myData.spells;
    delete myData.checks;
    myData.skills = this.sort(myData.skills);

    // group and sort defences correctly for display
    let tempdefences = [...myData.defences];
    myData.defences = [];
    myData.blocks = [];
    for (let item of tempdefences) {
      if (item.system.category == "block") {
        myData.blocks.push(item);
      } else {
        if (myData.useSineNomine) {
          item.system.value = 16 - item.system.value;
          item.system.moddedvalue = 16 - item.system.moddedvalue;
        } else {
          item.system.stepvalue = item.system.moddedvalue - item.system.value;
        }
        myData.defences.push(item);
      }
    }
    myData.attackvariables = [];
    for (let varname of rtw.rtw.attackvariables) {
      if (!varname.trim()) continue;
      if (actordata.dynamic[varname]) myData.attackvariables.push(this.actor.items.get(actordata.dynamic[varname]._id));
      else if (actordata.tracked[varname]) myData.attackvariables.push(this.actor.items.get(actordata.tracked[varname]._id));
    }
    myData.defencevariables = [];
    for (let varname of rtw.rtw.defencevariables) {
      if (!varname.trim()) continue;
      if (actordata.dynamic[varname]) myData.defencevariables.push(this.actor.items.get(actordata.dynamic[varname]._id));
      else if (actordata.tracked[varname]) myData.defencevariables.push(this.actor.items.get(actordata.tracked[varname]._id));
    }
    myData.skillvariables = [];
    for (let varname of rtw.rtw.skillvariables) {
      if (!varname.trim()) continue;
      if (actordata.dynamic[varname]) myData.skillvariables.push(this.actor.items.get(actordata.dynamic[varname]._id));
      else if (actordata.tracked[varname]) myData.skillvariables.push(this.actor.items.get(actordata.tracked[varname]._id));
    }
    myData.spellvariables = [];
    for (let varname of rtw.rtw.spellvariables) {
      if (!varname.trim()) continue;
      if (actordata.dynamic[varname]) myData.spellvariables.push(this.actor.items.get(actordata.dynamic[varname]._id));
      else if (actordata.tracked[varname]) myData.spellvariables.push(this.actor.items.get(actordata.tracked[varname]._id));
    }

    return context;
  }

  sort(data) {
    return data.sort((a, b) => {
      if (a.sort < b.sort) return -1;
      if (a.sort > b.sort) return 1;
      return 0;
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.importRtW').click(this._onImportRtWData.bind(this));
  }

  async _onImportRtWData(event) {
    event.preventDefault();
    let temp = this.actor.system.itemscript;
    if (temp[0] == "\"") { // remove the wrapping doublequotes
      temp = temp.substr(1, temp.length - 2);
    }
    const scriptdata = temp.split(/\r?\n/).map(word => word.trim());

    let biography = this.actor.system.biography;

    for (let entry of scriptdata) {
      let regex = new RegExp(/([^:]+):([^\r|\n]+)/gi);
      const line = regex.exec(entry);

      switch (line[1].trim()) {
        case "Name": {
          // Name: {{name}}
          await this.actor.update({ 'name': line[2].trim() });
          break;
        }
      }
    }
    await this.actor.update({ 'system.biography': biography });
    await this.actor.update({ 'system.itemscript': '' });
    ui.notifications.info("Stat block import complete");
    return;
  }

  async _onRoll(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;
    const token = game.canvas.tokens.controlled[0] || false;
    const actor = token ? token.actor._id == this.actor._id ? token.actor : this.actor : this.actor;

    const rolldata = {
      actor: actor,
      flavour: game.i18n.format(`rtw.phrases.${dataset.type}`, { name: dataset.name }),
      threat: Number(dataset.threat) || 12,
      type: dataset.type,
      modtype: dataset.modtype,
      roll: dataset.roll,
      name: dataset.name,
    }

    actor.roll(rolldata);
    return;
  }
}
