/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class baseActorSheet extends ActorSheet {

  itemContextMenu = [
    {
      name: game.i18n.localize("rtw.sheet.clone"),
      icon: '<i class="fas fa-copy"></i>',
      callback: (element) => {
        const item = foundry.utils.duplicate(this.actor.items.get(element.data("item-id")));
        delete item._id;
        delete item.sort;
        if (item.flags) delete item.flags;
        if (item.effects) delete item.effects;
        this.actor.createEmbeddedDocuments("Item", [item], {
          renderSheet: true,
        });
      },
    },
    {
      name: game.i18n.localize("rtw.sheet.edit"),
      icon: '<i class="fas fa-edit"></i>',
      callback: element => {
        const item = this.actor.items.get(element.data("item-id"));
        item.sheet.render(true);
      }
    },
    {
      name: game.i18n.localize("rtw.sheet.delete"),
      icon: '<i class="fas fa-trash"></i>',
      callback: element => {
        this.actor.deleteEmbeddedDocuments("Item", [element.data("item-id")]);
      }
    }
  ];

  /** @override */
  async getData() {
    if (CONFIG.rtw.testMode) console.debug("entering getData() in baseActor-sheet\n", this);

    // Basic data from dnd5e.mjs#10732
    let isOwner = this.actor.isOwner;
    const rollData = this.actor.getRollData.bind(this.actor);
    const context = {
      owner: isOwner,
      limited: this.actor.limited,
      options: this.options,
      editable: this.isEditable,
      cssClass: isOwner ? "editable" : "locked",
      isCharacter: this.actor.type === "Hero",
      isNPC: this.actor.type === "npc",
      isVehicle: this.actor.type === "vehicle",
      config: CONFIG.rtw,
      rollData
    };

    // The Actor's data
    const actor = this.actor;
    const actordata = actor.toObject(false);
    context.actor = actordata;
    context.system = actordata.system;
    context.items = actordata.items.sort((a, b) => {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });

    // My shortcuts
    const myData = context.system;

    myData.primaryattributes = [];
    myData.equipment = [];
    myData.toh = [];
    myData.hitlocations = [];
    myData.skills = [];
    myData.spells = [];
    myData.checks = [];
    myData.advantages = [];
    myData.disadvantages = [];
    myData.perks = [];
    myData.quirks = [];
    myData.attacks = [];
    myData.defences = [];
    myData.pools = [];
    myData.containers = [];
    myData.modifiers = [];
    myData.variables = [];
    myData.attackdamagemods = [];
    myData.defencemods = [];
    myData.reactionmods = [];
    myData.skillmods = [];
    myData.spellmods = [];
    myData.checkmods = [];
    myData.primods = [];

    for (let item of myData.items) {
      switch (item.type) {
        case "Primary-Attribute": {
          myData.primaryattributes.push(item);
          break;
        }
        case "Equipment": {
          myData.equipment.push(item);
          break;
        }
        case "Token-of-Heroism": {
          myData.toh.push(item);
          break;
        }
        case "Hit-Location": {
          myData.hitlocations.push(item);
          break;
        }
        case "Rollable": {
          switch (item.system.category) {
            case "skill":
            case "technique": {
              myData.skills.push(item);
              break;
            }
            case "spell":
            case "rms": {
              myData.spells.push(item);
              break;
            }
            case "check": {
              myData.checks.push(item);
              break;
            }
          }
          break;
        }
        case "Feature": {
          switch (item.system.category) {
            case "advantage": {
              myData.advantages.push(item);
              break;
            }
            case "disadvantage": {
              myData.disadvantages.push(item);
              break;
            }
            case "quirk": {
              myData.quirks.push(item);
              break;
            }
            case "perk": {
              myData.perks.push(item);
              break;
            }
          }
          break;
        }
        case "Weapon": {
          myData.attacks.push(item);
          break;
        }
        case "Defence": {
          myData.defences.push(item);
          break;
        }
        case "Pool": {
          myData.pools.push(item);
          if (item.system.abbr == "TOH") myData.TOH = item;
          break;
        }
        case "Container": {
          myData.containers.push(item);
          break;
        }
        case "Variable": {
          myData.variables.push(item);
          break;
        }
        case "Modifier": {
          myData.modifiers.push(item);
          if (item.system.attack || item.system.damage) myData.attackdamagemods.push(item);
          if (item.system.defence) myData.defencemods.push(item);
          if (item.system.reaction) myData.reactionmods.push(item);
          if (item.system.skill) myData.skillmods.push(item);
          if (item.system.spell) myData.spellmods.push(item);
          if (item.system.check) myData.checkmods.push(item);
          if (item.system.primary) myData.primods.push(item);
          break;
        }
      }
    }
    return context;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.item-create').click(this._onItemCreate.bind(this));
    html.find('.item-export').click(this._onItemExport.bind(this));
    html.find('.importJSON').click(this._onImportJSON.bind(this));

    html.find('.iteminput').change(this._onInputChange.bind(this));
    html.find('.plus').click(this._onPlusMinus.bind(this));
    html.find('.minus').click(this._onPlusMinus.bind(this));
    html.find('.rollable').click(this._onRoll.bind(this));
    html.find('.item-edit').click(this._onItemEdit.bind(this));
    html.find('.item-delete').click(this._onItemDelete.bind(this));
    html.find('.item-toggle').click(this._onToggleItem.bind(this));
    html.find('.collapsible').click(this._onToggleCollapse.bind(this));

    new ContextMenu(html, ".item", this.itemContextMenu);

    let handler = ev => this._onDragStart(ev);
    html.find('.draggable').each((i, li) => {
      li.setAttribute("draggable", true);
      li.addEventListener("dragstart", handler, false);
    });

    html.on('click', '.open-journal', ev => {
      ev.preventDefault(); // TODO: does this need to be here?
      const journal = ev.currentTarget.dataset.table;
      try {
        Journal._showEntry(game.journal.getName(journal).uuid, "text", true);
      } catch (err) {
        if (journal) ui.notifications.error(`There is no matching Journal Entry for ${journal}. Perhaps you have yet to import it from the compendium?`);
      }
    });

  }

  async _onImportJSON(event) {
    event.preventDefault();
    let scriptdata = this.actor.system.itemscript || "";
    // preserve newlines, etc - use valid JSON
    scriptdata = scriptdata.replace(/\\n/g, "\\n")
      .replace(/\\'/g, "\\'")
      .replace(/\\"/g, '\\"')
      .replace(/\\&/g, "\\&")
      .replace(/\\r/g, "\\r")
      .replace(/\\t/g, "\\t")
      .replace(/\\b/g, "\\b")
      .replace(/\\f/g, "\\f");
    // remove non-printable and other non-valid JSON chars
    scriptdata = scriptdata.replace(/[\u0000-\u0019]+/g, "");
    const data = JSON.parse(scriptdata);
    console.log("JSON Object\n", data);
  }

  _onToggleCollapse(event) {
    event.preventDefault();
    if (CONFIG.rtw.testMode) console.debug("entering _onToggleCollapse()", [this, event]);
    // do not collapse when creating an item
    if (event.target.parentElement.className.includes('item-create')) return;
    if (event.target.parentElement.className.includes('char-name')) return;
    let section = event.currentTarget.dataset.section;
    let current = this.actor.system.sections[section];
    let update = `system.sections.${section}`;
    let data = (current === "block") ? "none" : "block";
    this.actor.update({ [update]: data });
  }

  async _onPlusMinus(event) {
    event.preventDefault();

    if (CONFIG.rtw.testMode) console.debug("entering _onPlusMinus()", [this, event]);

    let field = event.currentTarget.firstElementChild;
    let actordata = this.actor.system;
    let fieldName = field.name;
    let change = Number(field.value);
    var value;
    var fieldValue;

    if (field.className.includes("pool")) {
      return this.actor.modifyTokenAttribute(`tracked.${fieldName.toLowerCase()}`, field.value, true, true);

    } else { // haven't figured out non-pools yet

      switch (fieldName) {
        case "gmod": {
          fieldValue = "system.gmod.value";
          value = change + actordata.gmod.value;
          this.actor.update({ [fieldValue]: value });
          break;
        }
        default: {
          break;
        }
      }
    }

  }

  _onToggleItem(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);
    let attr = "";
    switch (item.type) {
      case "Modifier": {
        attr = "system.inEffect";
        break;
      }
      default: {
        ui.notifications.warn(`Toggling of ${item.type} is not yet supported.`)
      }
    }
    return this.actor.updateEmbeddedDocuments("Item", [{ _id: itemId, [attr]: !foundry.utils.getProperty(item, attr) }]);
    return item.update({ [attr]: !foundry.utils.getProperty(item.system, attr) });
  }

  _onItemEdit(event) {
    event.preventDefault();
    let element = event.currentTarget;
    let itemId = element.closest(".item").dataset.itemId;
    let item = this.actor.items.get(itemId);
    item.sheet.render(true);
  }

  _onItemDelete(event) {
    event.preventDefault();
    let element = event.currentTarget;
    let itemId = element.closest(".item").dataset.itemId;
    this.actor.deleteEmbeddedDocuments("Item", [itemId]);
  }

  async dropItem(item) { // special container behaviour

    // the item has already been created on the actor sheet
    let baseitem = await this.actor.items.get(item[0].id);

    if (baseitem.type == "Container") {
      // unpack the container then delete it
      await this._createItems(baseitem.system.notes);
      this.actor.deleteEmbeddedDocuments("Item", [item[0].id]);
    } else {
      // show the item sheet
      // FIXME: don't open an item sheet that originated on the same actor sheet
      baseitem.sheet.render(true);
    }
    return baseitem;
  }

  /** @override */
  async _onDrop(event) {
    // Try to extract the data
    let dragData;
    try {
      dragData = JSON.parse(event.dataTransfer.getData('text/plain'));
    } catch (err) {
      return false;
    }
    const actor = this.actor;

    // a contained item or wound effect will not have a uuid
    const temp = dragData.uuid?.split(".") || false;

    let itemids = {};
    if (temp) {
      for (let i = 0; i < temp.length; i++) {
        itemids[temp[i]] = temp[++i];
      }
    }

    const sameActor = (actor.isToken) ? actor.uuid.includes(itemids.Token) : actor.id == itemids.Actor;

    // wait for the item to be copied to the actor
    let item = await super._onDrop(event);

    if (sameActor) return item;
    if (item) {
      // a proper item was dropped and identified so unpack it or render it
      return this.dropItem(item);
    } else {
      // a piece of non-item data was dropped
      this.dropData(dragData);
    }
  }

  async _onTokenDrop(dragItem) {
    // wait for the item to be copied to the actor
    if (dragItem.type == "Item") {
      let item = await this._onDropItem(null, dragItem);
      return this.dropItem(item);
    } else {
      if (CONFIG.rtw.testMode) console.debug("Data drop on Token not handled by baseActorSheet");
    }
  }

  _onInputChange(event) {
    event.preventDefault();

    if (CONFIG.rtw.testMode) console.debug("entering _onInputChange()", [this, event]);

    const target = event.currentTarget;
    // get the item id
    const itemId = target.parentElement.attributes["data-item-id"]?.value;
    // get the name of the changed element
    let dataname = target.attributes["data-name"].value;
    // get the new value
    let value = (target.type === "checkbox") ? target.checked : target.value;
    // Get the original item.
    const item = this.actor.items.get(itemId);

    if (dataname == "system.alwaysOn") {
      item.update({ "system.alwaysOn": value, "system.inEffect": true });
      return;
    }

    // redirect changes to pool values
    if (target.className.includes("pool")) {
      let isDelta = (value.startsWith("+") || value.startsWith("-"));
      return this.actor.modifyTokenAttribute(`tracked.${target.dataset.abbr.toLowerCase()}`, value, isDelta, true);
    }

    // update the item with the new value for the element or the actor if it is gmod
    (item) ? item.update({ [dataname]: value }) : this.actor.update({ [dataname]: value });
  }

  _onItemCreate(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;
    const scriptdata = this.actor.system.itemscript || "";
    if (!dataset.type) {
      this._createItems(scriptdata);
    } else {
      this._createItem(dataset);
    }
  }

  _onItemExport() {
    let items = foundry.utils.duplicate(this.actor.system.items);
    var exported = [];
    for (var itemdata of items) {
      if (itemdata.type == "Container") continue;
      delete itemdata._id;
      delete itemdata.flags;
      delete itemdata.sort;
      delete itemdata.effects;
      exported.push(itemdata);
    }
    exported = JSON.stringify(exported);
    this.actor.update({ "system.itemscript": exported });
  }

  async _createItems(scriptdata) {
    // preserve newlines, etc - use valid JSON
    scriptdata = scriptdata.replace(/\\n/g, "\\n")
      .replace(/\\'/g, "\\'")
      .replace(/\\"/g, '\\"')
      .replace(/\\&/g, "\\&")
      .replace(/\\r/g, "\\r")
      .replace(/\\t/g, "\\t")
      .replace(/\\b/g, "\\b")
      .replace(/\\f/g, "\\f");
    // remove non-printable and other non-valid JSON chars
    scriptdata = scriptdata.replace(/[\u0000-\u0019]+/g, "");
    await this.actor.createEmbeddedDocuments('Item', JSON.parse(scriptdata));
    await this.actor.update({ 'system.itemscript': '' });
    ui.notifications.info("Creation of multiple items completed!");
  }

  async _createItem(dataset) {
    const data = {
      name: dataset.type,
      type: dataset.type,
      system: {}
    };
    const display = dataset.display === undefined ? true : dataset.display;
    const myData = data.system;
    myData.category = dataset.category || null;
    if (dataset.type == "Modifier") {
      myData.attack = (dataset.mod1 == "attack" || dataset.mod2 == "attack");
      myData.defence = (dataset.mod1 == "defence" || dataset.mod2 == "defence");
      myData.skill = (dataset.mod1 == "skill" || dataset.mod2 == "skill");
      myData.spell = (dataset.mod1 == "spell" || dataset.mod2 == "spell");
      myData.check = (dataset.mod1 == "check" || dataset.mod2 == "check");
      myData.reaction = (dataset.mod1 == "reaction" || dataset.mod2 == "reaction");
      myData.damage = (dataset.mod1 == "damage" || dataset.mod2 == "damage");
      myData.primary = (dataset.mod1 == "primary" || dataset.mod2 == "primary");
    } else if (dataset.type == 'Points') {
      myData.stat = dataset.stat || "hp";
      myData.type = dataset.method || (dataset.stat == "hp") ? "damage" : "spend";
      myData.qty = dataset.qty || 1;
    }
    return await this.actor.createEmbeddedDocuments('Item', [data], { renderSheet: display });
  }

  /**
   * Fetches the formatted modifiers for a roll.
   */
  fetchRelevantModifiers(actordata, dataset) {
    let tempMods = [];
    const actormods = actordata.items.filter(i => (i.type == "Modifier"));
    const type = (dataset.type == "modlist") ? dataset.modtype : dataset.type;

    // preload gmod
    tempMods.push({ modifier: Number(actordata.gmod.value) || 0, description: 'global modifier' });

    // iterate through the modifiers in effect, picking the appropriate ones for the roll
    for (const mod of actormods) {
      const moddata = mod.system;

      if (moddata.inEffect) {

        for (const entry of moddata.entries) {
          // check to see if this entry applies to this type of roll
          switch (type) {
            case "technique":
              if (entry.category != "skill") continue;
              break;
            case "rms":
              if (entry.category != "spell") continue;
              break;
            case "dodge":
            case "block":
            case "parry":
              if (entry.category != "defence") continue;
              break;
            default:
              if (entry.category != type) continue;
          }
          if (Number(entry.moddedformula) == 0) continue;

          // what is the target of this modifier?
          let hasRelevantTarget = entry.targets.trim();
          // does this modifier have a target matching the item being rolled?
          if (hasRelevantTarget != "") {
            hasRelevantTarget = false;
            // get the array of targets to which it applies
            const cases = entry.targets.split(",").map(word => word.trim());
            for (const target of cases) {
              // test the target against the beginning of dataset.name for a match
              if (dataset.name.startsWith(target)) {
                hasRelevantTarget = true;
                continue;
              }
            }
          } else {
            // a general modifier
            hasRelevantTarget = true;
          }

          if (hasRelevantTarget) {
            let clean = entry.moddedformula?.replace(/\(([-+][\d]+)\)/g, "$1");
            tempMods.push({
              modifier: (clean == undefined) ? entry.value : clean[0] == "+" || clean[0] == "-" ? clean : "+" + clean,
              description: moddata.tempName
            });
          }
        }
      }
    }

    const prepareModList = mods => mods.map(mod => ({ ...mod, modifier: mod.modifier })).filter(mod => mod.modifier !== 0);

    return prepareModList(tempMods);
  }
}