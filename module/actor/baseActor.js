import { MyDialog } from "../dialog.js";

/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class BaseActor extends Actor {
  /**
   * Fetch and sort all the items, even though they will be prepared internally after this stage.
   */
  prepareBaseData() {
    const myData = this.system;
    if (CONFIG.rtw.testMode)
      console.debug("entering prepareBaseData()\n", [this, myData]);

    myData.items = Array.from(this.items.values()).sort((a, b) => {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });

    /**
     * Reference data structure to mimic static systems
     */
    myData.defence = {};
    myData.points = {};
    myData.ingredients = {};
    myData.dynamic = {};
    myData.modifiers = {};
    myData.tracked = {};
  }

  /**
   * Augment the basic actor data with additional dynamic data.
   *
   * Changes made here to item seem to stick.
   */
  prepareDerivedData() {
    const myData = this.system;

    if (CONFIG.rtw.testMode) console.debug("entering prepareDerivedData()\n", [this, myData]);

    // group the items for efficient iteration
    const actormods = []; // itemdata
    const primods = []; // itemdata
    const actornonmods = []; // item
    const actorrollables = []; // item
    const weapons = []; // item
    let toh_max = 0;

    // store references to primary attribute and pool items
    let previousLayer = [];
    let nextLayer = [];

    for (let [id, trait] of Object.entries(myData.traits)) {
      // value is for references, moddedvalue is the modified ranks
      trait.value = Number(trait.ranks);
      myData.dynamic[id] = {
        name: trait.name,
        abbr: id,
        aspect: trait.aspect,
        ranks: trait.ranks,
        value: trait.value,
        type: "attribute"
      };
      nextLayer.push(id);
    }

    // write the items into the model
    for (const item of myData.items) {
      const itemdata = item.system;
      // collect non-modifier items with formula
      if (itemdata.formula) actornonmods.push(item);

      switch (item.type) {
        case "Pool": {
          let itemID = itemdata.abbr.toLowerCase();
          itemdata.hasmin = false;
          itemdata.hasmax = false;

          // write the pool to tracked so it can be seen by the token
          myData.tracked[itemID] = {
            _id: item._id,
            abbr: itemdata.abbr,
            name: item.name,
            state: itemdata.state,
            min: Number(itemdata.min),
            value: Number(itemdata.value),
            max: Number(itemdata.max),
            notes: itemdata.notes,
            type: item.type
          };
          if (itemID) actornonmods.push(item);
          break;
        }
        case "Modifier": {
          let itemID = this.slugify(item.name);
          myData.modifiers[itemID] = item;
          actormods.push(itemdata);
          if (itemdata.primary) primods.push(itemdata);
          itemdata.tempName = item.name;
          break;
        }
        case "Points": {
          myData.points[item._id] = item;
          break;
        }
        case "Ingredient": {
          myData.ingredients[item._id] = item;
          break;
        }
        case "Weapon": {
          // write these items to dynamic
          let itemID = this.slugify(item.name);
          myData.dynamic[itemID] = {
            name: item.name,
            acc: itemdata.acc,
            atk: itemdata.atk,
            dur: itemdata.dur,
            maxdur: itemdata.maxdur,
            rank: itemdata.rank,
            material: itemdata.material,
            style: itemdata.style,
            ench: itemdata.ench,
            notes: itemdata.notes,
            _id: item._id,
            type: item.type
          };
          weapons.push(item);
          break;
        }
        case "Defence":
        case "Rollable": {
          // write these items to dynamic
          let itemID = this.slugify(item.name);
          myData.dynamic[itemID] = {
            name: item.name,
            formula: itemdata.formula,
            category: itemdata.category,
            value: Number(itemdata.value),
            _id: item._id,
            notes: itemdata.notes,
            type: item.type
          };

          if (Number.isNumeric(itemdata.formula) || itemdata.formula.startsWith("#")) nextLayer.push(itemID);
          actorrollables.push(item);
          break;
        }
        case "Token-of-Heroism": {
          // count the values of tokens_of_heroism to produce the max value
          toh_max += Number(item.system.value);
          break;
        }
        case "Hit-Location": {
          break;
        }
        case "Variable": {
          // write these items to dynamic
          let itemID = this.slugify(item.name);
          myData.dynamic[itemID] = {
            name: item.name,
            value: Number(itemdata.value),
            formula: itemdata.formula,
            label: itemdata.label,
            _id: item._id,
            notes: itemdata.notes,
            type: item.type
          };
          // add the term to the layer if the formula is a number
          if (Number.isNumeric(itemdata.formula)) nextLayer.push(itemID);

          break;
        }
        default: { // Features that define a vision type for the actor
          switch (item.name) {
            case "Keen Senses":
            case "Night Sight":
            case "Dark Sight":
            case "Darkvision":
            case "Star Sight": {
              myData.dynamic.visionType = item.name;
            }
          }
        }
      }
    }
    // Set the max value of the Tokens of Heroism Pool to be the sum of the ToH counted
    if (myData.tracked.toh)
      this.updateEmbeddedDocuments("Item", [{ _id: myData.tracked.toh._id, system: { maxForm: `${toh_max}` } }]);

    // straight to number formula processing
    for (const item of actornonmods) {
      const itemdata = item.system;
      if (item.type == "Pool") { // new formula behaviour for min and max
        let dynID = itemdata.abbr.toLowerCase();
        if (Number.isNumeric(itemdata.maxForm)) {
          // write to Actor Item
          itemdata.max = Number(itemdata.maxForm);
          // write to tracked
          myData.tracked[dynID].max = itemdata.max;
          itemdata.hasmax = true;
        }
        if (Number.isNumeric(itemdata.minForm)) {
          // write to Actor Item
          itemdata.min = Number(itemdata.minForm);
          // write to tracked
          myData.tracked[dynID].min = itemdata.min;
          itemdata.hasmin = true;
        }
        if (itemdata.hasmax && itemdata.hasmin) nextLayer.push(dynID);
      } else {
        if (Number.isNumeric(itemdata.formula)) {
          let dynID = this.slugify(item.name);
          // write to Actor Item
          itemdata.value = Number(itemdata.formula);
          // write to dynamic
          myData.dynamic[dynID].value = itemdata.value;
        }
      }
    }

    // Primary Attribute modifiers first
    for (const itemdata of primods) {
      // the modifier has a primod and is in effect
      if (itemdata.inEffect) {
        for (const entry of itemdata.entries) {
          // this entry is a primod
          if (entry.category == "primary") {
            // process primods formulae before the mods are applied
            if (Number.isNumeric(entry.formula)) {
              entry.value = Number(entry.formula);
            } else {
              if (entry.formula == "") continue;
              let formData = this._replaceData(entry.formula);
              try {
                entry.value = Math.round(eval(formData.value.replace(CONFIG.rtw.dataRgx, '')));
                entry.moddedformula = "" + entry.value; // store the result as a String
              } catch (err) {
                // store the formula ready to be rolled
                entry.moddedformula = formData.value;
                entry.value = 0; // eliminate any old values
                console.debug("Primary Modifier formula evaluation error:\n", [entry.moddedformula]);
              }
              if (!itemdata.tempName.includes(formData.label)) itemdata.tempName += formData.label;
            }
            const cases = entry.targets.split(",").map(word => word.trim().toLowerCase());
            for (const target of cases) {
              // we have found the target in dynamic
              if (myData.dynamic[target]) {

                // write to traits
                let origitem = myData.traits[target];
                origitem.value = Number(origitem.value) + entry.value;

                // write to dynamic
                myData.dynamic[target].value = origitem.value;
              }
            }
          }
        }
      }
    }

    // initialise the secondary stats
    for (let [id, stat] of Object.entries(myData.secondaries)) {
      let factor = 4;
      if (id == "hp" && myData.dynamic.sizefactor) {
        factor = factor * myData.dynamic.sizefactor.value || 4;
      }
      stat.value = stat.basemax = stat.max = myData.traits[stat.trait].value * factor;
      stat.temp = 0;
    }
    // update points to be applied from the array of Points
    for (let [id, points] of Object.entries(myData.points)) {
      const stat = myData.secondaries[points.system.stat];
      switch (points.system.type) {
        case "damage":
        case "bind":
        case "spend": {
          stat.value -= points.system.qty;
          break;
        }
        case "burn": {
          stat.value -= points.system.qty;
          stat.max -= points.system.qty;
          break;
        }
        case "temp": {
          let change = 0;
          change = Math.min(points.system.qty, stat.basemax - stat.temp);
          stat.temp += change;
          stat.max += change;
          stat.value += change;
          break;
        }
      }
    }

    // calculate the remaining formulae one layer at a time
    while (nextLayer.length !== 0) {
      previousLayer = nextLayer;
      nextLayer = [];

      for (const item of actornonmods) {
        const itemdata = item.system;
        if (item.type == "Pool") { // new pool formula functionality
          //ignore numeric formulae
          if (itemdata.hasmax && itemdata.hasmin) continue;

          let dynID = itemdata.abbr.toLowerCase();

          if (!itemdata.hasmax) { // formula has not yet been calculated
            for (const target of previousLayer) {
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.maxForm.includes(target)) {
                // write to Actor Item
                let formData = this._replaceData(itemdata.maxForm);
                try {
                  itemdata.max = Math.round(eval(formData.value.replace(CONFIG.rtw.dataRgx, '')) * 100) / 100;
                } catch (err) {
                  itemdata.max = 0; // eliminate any old values
                  console.warn("Pool-max formula evaluation error:\n", [this.name, item.name, itemdata.maxForm]);
                }
                // write to tracked
                myData.tracked[dynID].max = itemdata.max; // this pushes a new value to the target, not the item
                itemdata.hasmax = true;
              }
            }
          }
          if (!itemdata.hasmin) { // formula has not yet been calculated
            for (const target of previousLayer) {
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.minForm.includes(target)) {
                // write to Actor Item
                let formData = this._replaceData(itemdata.minForm);
                try {
                  itemdata.min = Math.round(eval(formData.value.replace(CONFIG.rtw.dataRgx, '')) * 100) / 100;
                } catch (err) {
                  itemdata.min = 0; // eliminate any old values
                  console.warn("Pool-min formula evaluation error:\n", [this.name, item.name, itemdata.minForm]);
                }
                // write to tracked
                myData.tracked[dynID].min = itemdata.min; // this pushes a new value to the target, not the item
                itemdata.hasmin = true;
              }
            }
          }
          if (itemdata.hasmax && itemdata.hasmin) nextLayer.push(dynID);
        } else {
          //ignore items with no formula or whose formula has already been processed
          if (!itemdata.formula || Number.isNumeric(itemdata.formula) || itemdata.formula.startsWith("#") || (Number(itemdata.moddedformula) == itemdata.value && itemdata.moddedformula != "")) continue;

          let dynID = this.slugify(item.name);
          if (previousLayer.includes(dynID)) continue;

          // the formula is a reference
          if (itemdata.formula.includes("@")) {
            for (const target of previousLayer) {

              // if this formula refers to an item in the previous layer then process it
              if (itemdata.formula.includes(target)) {

                // write to Actor Item
                let formData = this._replaceData(itemdata.formula);
                try {
                  itemdata.value = Math.round(eval(formData.value.replace(CONFIG.rtw.dataRgx, '')) * 100) / 100;
                  itemdata.moddedformula = "" + itemdata.value; // store the result as a String
                } catch (err) {
                  // store the formula ready to be rolled
                  itemdata.moddedformula = formData.value;
                  itemdata.value = 0; // eliminate any old values
                  console.warn("Non-Modifier formula evaluation incomplete:\n", [this.name, item.name, itemdata.moddedformula]);
                }

                // write to dynamic
                myData.dynamic[dynID].moddedformula = itemdata.moddedformula;
                myData.dynamic[dynID].value = itemdata.value; // this pushes a new value to the target, not the item

                // put this item into the next layer
                nextLayer.push(dynID);
              }
            }
          } else { // the formula might be a dice expression
            if (itemdata.formula.toLowerCase().includes("d")) {
              itemdata.moddedformula = itemdata.formula;
              itemdata.value = 0; // eliminate any old values
              console.debug("Encountered a Dice Expression:\n", [itemdata.moddedformula]);
              // write to dynamic
              myData.dynamic[dynID].moddedformula = itemdata.moddedformula;
              myData.dynamic[dynID].value = itemdata.value; // this pushes a new value to the target, not the item
            }
          }
        }
      }
    }

    // process all modifiers again after the non-modifiers have been processed
    for (const itemdata of actormods) {
      for (const entry of itemdata.entries) {
        if (Number.isNumeric(entry.formula)) {
          entry.value = Number(entry.formula);
        } else {
          if (entry.formula == "") continue;
          let formData = this._replaceData(entry.formula);
          try {
            entry.value = Math.round(eval(formData.value.replace(CONFIG.rtw.dataRgx, '')));
            entry.moddedformula = "" + entry.value; // store the result as a String
          } catch (err) {
            // store the formula ready to be rolled
            entry.moddedformula = formData.value;
            entry.value = 0; // eliminate any old values
            console.warn("Modifier formula evaluation error:\n", [this.name, itemdata.tempName, entry.moddedformula]);
          }
          if (!itemdata.tempName.includes(formData.label)) itemdata.tempName += formData.label;
        }
      }
    }

    // calculate the modified value of the item
    for (const item of actorrollables) {
      const itemdata = item.system;
      itemdata.moddedvalue = itemdata.value;
      // if there is no modified formula, make it the value
      if (itemdata.moddedformula == undefined) itemdata.moddedformula = "" + itemdata.value;
      let itemID = this.slugify(item.name);
      switch (item.type) {
        case "Weapon": {
          itemdata.moddedacc += this.fetchRelevantModifiers(item.name, "attack");
          itemdata.moddedatk += this.fetchRelevantModifiers(item.name, "damage");
          break;
        }
        case "Defence": {
          itemdata.moddedvalue += this.fetchRelevantModifiers(item.name, "defence");
          break;
        }
        case "Rollable": {
          switch (itemdata.category) {
            case "check": {
              itemdata.moddedvalue += this.fetchRelevantModifiers(item.name, "reaction");
              break;
            }
            case "technique":
            case "skill": {
              itemdata.moddedvalue += this.fetchRelevantModifiers(item.name, "skill");
              break;
            }
            case "rms":
            case "spell": {
              itemdata.moddedvalue += this.fetchRelevantModifiers(item.name, "spell");
              break;
            }
          }
          break;
        }
      }
      myData.dynamic[itemID].moddedvalue = itemdata.moddedvalue;
    }

    // calculate the modified values for weapons
    for (const item of weapons) {
      const itemdata = item.system;
      let itemID = this.slugify(item.name);
      itemdata.moddedacc = itemdata.acc;
      itemdata.moddedacc += this.fetchRelevantModifiers(item.name, "attack");
      myData.dynamic[itemID].moddedacc = itemdata.moddedacc;
      itemdata.moddedatk = itemdata.atk;
      itemdata.moddedatk += this.fetchRelevantModifiers(item.name, "damage");
      myData.dynamic[itemID].moddedatk = itemdata.moddedatk;
    }
  }

  async getNamedItem(name) {
    let parts = name.split(".");
    if (parts.length != 2) return undefined;
    return await this.items.get(this.system[parts[0]][parts[1]]?._id);
  }

  slugify(text) {
    return text
      .toString()                     // Cast to string
      .toLowerCase()                  // Convert the string to lowercase letters
      .normalize('NFD')       // The normalize() method returns the Unicode Normalization Form of a given string.
      .trim()                         // Remove whitespace from both sides of a string
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-');        // Replace multiple - with single -
  }

  /**
   * Replace referenced data attributes in the formula with the syntax `@attr` with the corresponding key from
   * the provided `data` object.
   * @param {String} formula    The original formula within which to replace
   * @private
   */
  _replaceData(formula) {
    let dataRgx = /[@|#]([\w.\-]+)/gi;
    let data = this.system;
    let tempName = "";
    let findTerms = (match, term) => {
      switch (match[0]) {
        case "@": {
          let value = data.dynamic[term]?.value || data.tracked[term]?.value;
          if (value == undefined) value = data.dynamic[term]?.moddedformula;
          let label = data.dynamic[term]?.label;
          tempName += (label) ? ` (${label})` : "";
          return (value) ? String(value).trim() : "0";
        }
        case "#": {
          return this.rankCalculator(Number(term));
        }
      }
    };

    const replyData = formula.replace(dataRgx, findTerms).replace(/(min)|(max)|(floor)|(ceil)|(round)|(abs)|(pow)/g, "Math.$&");
    return { value: replyData, label: tempName };
  }

  async setConditions(newValue, attrName) {
    if (CONFIG.rtw.testMode) console.debug("entering setConditions()\n", [newValue, attrName]);

    const tracked = this.system.tracked;
    const attr = attrName.split('.')[2];
    const item = this.items.get(tracked[attr]._id);
    const itemdata = item.system;
    let attrValue = itemdata.value;
    let attrMax = itemdata.max;
    let attrState = itemdata.state;
    let attrMin = itemdata.min;

    // Assign the variables
    if (attrName.includes('.max')) {
      attrMax = Math.round(eval(this._replaceData(newValue).value.replace(CONFIG.rtw.dataRgx, '')));
    } else if (attrName.includes('.min')) {
      attrMin = Math.round(eval(this._replaceData(newValue).value.replace(CONFIG.rtw.dataRgx, '')));
    } else {
      attrValue = newValue;
    }
    const ratio = attrValue / attrMax;

    switch (attr) {
      case "hp": {
        switch (Math.trunc(ratio * 8)) {
          case 1: {
            attrState = '[1/8]';
            break;
          }
          case 2: {
            attrState = '[1/4]';
            break;
          }
          case 3: {
            attrState = '[3/8]';
            break;
          }
          case 4: {
            attrState = '[1/2]';
            break;
          }
          case 5: {
            attrState = '[5/8]';
            break;
          }
          case 6: {
            attrState = '[3/4]';
            break;
          }
          case 7: {
            attrState = '[7/8]';
            break;
          }
          case 8: {
            attrState = '[FIT]';
            break;
          }
          default: { // bad shape
            if (attrValue <= attrMin) {
              attrState = '[DEAD]';
            } else if (attrValue <= 0) {
              attrState = '[UNC]';
            } else {
              attrState = '[< 1/8]';
            }
          }
        }
        break;
      }
      case "fp": {
        // set the limits
        switch (Math.trunc(ratio)) {
          case 0: {
            if (ratio <= 0) { // collapse
              attrState = '[C]';
              break;
            } else if (attrValue < (attrMax / 3)) { // tired
              attrState = '[T]';
              break;
            }
            // fresh, no break
          }
          case 1: { // fresh
            attrState = '[F]';
            break;
          }
          default: { // unconscious
            attrState = '[UNC]';
            break;
          }
        }
        break;
      }
      default: { // assume that the state is measured in eighths
        switch (Math.trunc(ratio * 8)) {
          case 1: {
            attrState = '[1/8]';
            break;
          }
          case 2: {
            attrState = '[1/4]';
            break;
          }
          case 3: {
            attrState = '[3/8]';
            break;
          }
          case 4: {
            attrState = '[1/2]';
            break;
          }
          case 5: {
            attrState = '[5/8]';
            break;
          }
          case 6: {
            attrState = '[3/4]';
            break;
          }
          case 7: {
            attrState = '[7/8]';
            break;
          }
          case 8: {
            attrState = '[Full]';
            break;
          }
          default: { // dead
            if (ratio <= 0) { // empty
              attrState = '[Empty]';
            } else {
              attrState = '[< 1/8]';
            }
          }
        }
      }
    }
    itemdata.min = attrMin;
    itemdata.value = attrValue;
    itemdata.max = attrMax;
    itemdata.state = attrState;

    return await item.update({ system: itemdata });
  }

  /**
   * Handle how changes to a Token attribute bar are applied to the Actor.
   * Logic for pools to be edited by plus-minus and text input (on Actor and Item) is also redirected here.
   * @param {string} attribute    The attribute path
   * @param {number} value        The target attribute value
   * @param {boolean} isDelta     Whether the number represents a relative change (true) or an absolute change (false)
   * @param {boolean} isBar       Whether the new value is part of an attribute bar, or just a direct value
   * @return {Promise}
   */
  async modifyTokenAttribute(attribute, value, isDelta = false, isBar = true) {
    if (CONFIG.rtw.testMode) console.debug("entering modifyTokenAttribute()\n", [attribute, value, isDelta, isBar]);

    // fetch the pool if the attribute changing is 'value', or the attribute itelf if not
    const current = foundry.utils.getProperty(this.system, attribute);

    /**
     * Secondaries require Points to be created to modify them.
     */
    const attrparts = attribute.split(".");
    if (attrparts[0] == "secondaries") {
      if (!isDelta) return;
      const dataset = {
        type: "Points",
        qty: value * -1,
        stat: attrparts[1],
        display: false
      };
      return await this.sheet._createItem(dataset);
    }
    /**
     * Only Pool items get past here; secondaries will break it.
     * 
     * isBar is true for the value and must be clamped
     * isBar is false for the min or max
     */
    value = (isBar) ? (Math.clamp(current.min, (isDelta) ? Number(current.value) + Number(value) : Number(value), current.max)) : ((isDelta) ? Number(current) + Number(value) : value);
    // redirect updates to setConditions
    return await this.setConditions(value, `system.${attribute}`);
  }

  /**
   * Fetches the total modifier value for a rollable item
   */
  fetchRelevantModifiers(name, type) { // only attack, defence, reaction, skill & spell
    let tempMods = Number(this.system.gmod.value);
    const actormods = this.items.filter(i => (i.type == "Modifier"));

    // iterate through the modifiers in effect, picking the appropriate ones for the roll
    for (const mod of actormods) {
      const moddata = mod.system;

      if (moddata.inEffect) {

        for (const entry of moddata.entries) {
          // check to see if this entry applies to this type of roll
          if (type != entry.category) continue;

          // what is the target of this modifier?
          let hasRelevantTarget = entry.targets.trim();
          // does this modifier have a target matching the item being rolled?
          if (hasRelevantTarget != "") {
            hasRelevantTarget = false;
            // get the array of targets to which it applies
            const cases = entry.targets.split(",").map(word => word.trim());
            for (const target of cases) {
              // test the target against the beginning of dataset.name for a match
              if (name.startsWith(target)) {
                hasRelevantTarget = true;
                continue;
              }
            }
          } else {
            // a general modifier
            hasRelevantTarget = true;
          }
          tempMods += (hasRelevantTarget ? entry.value : 0);
        }
      }
    }
    return tempMods;
  }

  /**
   * Resets all temporary Variables and Modifiers
   */
  resetModVars(temporary = true) {

    const items = this.items.filter(function (item) {
      if (temporary) {
        return item.system.temporary;
      } else {
        return item.system.once;
      }
    });
    let updates = [];

    // iterate through the modifiers in effect, picking the appropriate ones for the roll
    for (const item of items) {
      switch (item.type) {
        case "Modifier": {
          updates.push({
            _id: item.id,
            ["system.inEffect"]: false
          });
          break;
        }
        case "Variable": {
          updates.push({
            _id: item.id,
            ["system.value"]: item.system.entries[1].value,
            ["system.formula"]: item.system.entries[1].formula,
            ["system.label"]: item.system.entries[1].label
          });
          break;
        }
      }
    }
    this.updateEmbeddedDocuments("Item", updates);
  }

  /**
   * A method to dispatch non-event-driven rolls.
   */
  async roll(rolldata) {
    let flavour = rolldata.flavour || game.i18n.format(`rtw.phrases.${rolldata.type}`, { name: rolldata.name });
    let threat = Number(rolldata.threat) || 12;
    let fail = 2;
    let canCrit = false, isModList = false;
    let rolledInitiative = rolldata.name == "Initiative";
    let hideTotal = false;

    switch (rolldata.type) {
      case "technique": { // send the notes to the chat and return
        const isplaintext = rolldata.roll.replace(/<[^>]*>?/gm, '') == rolldata.roll;
        ChatMessage.create({
          speaker: ChatMessage.getSpeaker({ actor: rolldata.actor }),
          flavor: flavour,
          content: isplaintext ? rolldata.roll.replace(/(?:\r\n|\r|\n)/g, '<br>') : rolldata.roll,
        });
        return;
      }
      case "rms": { // no roll, just storing a value
        return;
      }
    }

    // get the modifiers
    let modList = rolldata.actor.sheet.fetchRelevantModifiers(rolldata.actor.system, { type: rolldata.type, modtype: rolldata.modtype, name: rolldata.name });
    // process the modifiers
    let modformula = "";
    flavour += (rolldata.type != "modlist") ? ` [<b>${rolldata.roll}</b>]` : `<p class="chatmod">[<b>${rolldata.roll}</b>]: ${rolldata.name}<br>`;
    let hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      flavour += (rolldata.type != "modlist") ? `<p class="chatmod">` : "";
      for (const mod of modList) {
        sign = typeof mod.modifier === 'string' ? "" : mod.modifier > -1 ? "+" : "";
        modformula += `${sign}${mod.modifier}`;
        flavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    var formula = "";
    switch (rolldata.type) {
      case "modlist": // this will render a dialog with the modifiers, not a chat message
        isModList = true;
      case "block": // send the value to the chat as a modifiable roll
      case "damage": {// roll damage using a valid dice expression
        formula = rolldata.roll;
        break;
      }
      case "check": { // a valid dice expression
        formula = rolldata.roll;
        break;
      }
      case "parry": // 2d6 rolls not subject to crits
      case "spell": {
        formula = "2d6 + " + rolldata.roll;
        break;
      }
      case "attack": // 2d6 rolls subject to crits
      case "trait":
      case "dodge":
      case "skill": {
        formula = "2d6 + " + rolldata.roll;
        canCrit = true;
        break;
      }
      default: { // there should not be any of these
        console.error("Failed to process this roll type: " + rolldata.type);
      }
    }

    // process the modified roll
    let r = await new Roll(formula + modformula).evaluate();

    if (isModList) { // render the modlist dialog and return
      flavour += `<hr><p>${r.result} = <b>${r.total}</b></p>`;
      new MyDialog({
        title: rolldata.actor.name,
        content: flavour,
        buttons: {
          close: {
            icon: "<i class='fas fa-check'></i>",
            label: "Close"
          },
        },
        default: "close"
      }).render(true)
      return;
    }

    if (canCrit) { // an attack or defence roll
      let critfail = false;
      let critsuccess = false;
      if (r.terms[0].total <= fail) {
        critfail = true;
      } else if (r.terms[0].total >= threat) {
        critsuccess = true;
      }
      flavour += ` <p>2D6 Roll: [<b>${r.terms[0].total}</b>]`;
      if (critsuccess) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
      else if (critfail) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
      flavour += `</p>`
    }

    // prepare the flavor in advance based on which type of die is in use.
    r.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: rolldata.actor }),
        flavor: flavour,
        flags: { hideMessageContent: hideTotal }
      }
    );
    // prepare actor data updates
    let updates = { ["system.gmod.value"]: 0 };
    if (rolledInitiative) {
      updates["system.bs.value"] = r.total;
    }
    // reset all "once" modifiers
    rolldata.actor.resetModVars(false);
    rolldata.actor.update(updates);
    return;
  }

  // the flexible entry method for demanding users
  rollables(macroData) {
    macroData.position = macroData.position || { width: 300, top: 0, left: 0 };
    macroData.type = macroData.type || "";
    macroData.category = macroData.category || "";
    macroData.target = macroData.target || "";
    macroData.flavour = macroData.flavour || "";

    if (macroData.type == "Trait") {
      this.rollabledialog(this.system.traits, macroData);
    } else {
      const rollables = this.items.filter(function (item) {
        if (macroData.target != "") {
          const cases = macroData.target.split(",").map(word => word.trim());
          if (!cases.some(target => item.name.includes(target))) return null;
        }
        if (macroData.category != "") {
          const cases = macroData.category.split(",").map(word => word.trim());
          if (!cases.some(target => item.system.category?.includes(target))) return null;
        } else {
          const cases = macroData.type.split(",").map(word => word.trim());
          if (!cases.some(target => item.type.includes(target))) return null;
        }
        return item;
      });
      //console.log(`Rollables found by filter:\n`, {macroData,rollables});
      this.rollabledialog(rollables, macroData);
    }
  };

  // the initial selection dialog
  async rollabledialog(rollabledata, macroData) {
    const actor = this;
    const template = "systems/reclaim-the-wild/templates/rollmacro/rollables.hbs";
    const html = await renderTemplate(template, { rollabledata });

    new MyDialog({
      title: "Rollables",
      content: html,
      buttons: {},
      options: {
        actor: actor,
        macroData: macroData
      }
    }, {
      width: macroData.position.width || 300,
      top: macroData.position.top || 0,
      left: macroData.position.left || 0
    }).render(true);
  }

  // the actionable dialog with all relevant modifiers displayed
  async modifierdialog(rollableId, macroData) {
    const actor = this;
    let rollabledata = this.items.get(rollableId);
    if (rollableId == "" && macroData.type == "Trait") {
      rollabledata = actor.system.traits[macroData.target.toLowerCase()];
      rollabledata.type = macroData.type;
      rollabledata.system = { category: "trait" };
    }
    const type = rollabledata.type;
    const name = rollabledata.name;
    const category = rollabledata.system.category || null;

    const modifiers = [];
    for (const mod of Object.values(this.system.modifiers)) {
      for (const entry of mod.system.entries) {
        if (entry.formula == "") continue;
        // check to see if this entry applies to this type of roll
        switch (type) {
          case "Weapon": {
            if (!['damage', 'attack'].includes(entry.category)) continue;
            break;
          }
          case "Trait":
          case "Defence":
          case "Rollable": {
            if (category != entry.category) continue;
            break;
          }
        }

        let hasRelevantTarget = entry.targets.trim();
        // does this modifier have targets?
        if (hasRelevantTarget != "") {
          hasRelevantTarget = false;
          // get the array of targets to which it applies
          const cases = entry.targets.split(",").map(word => word.trim());
          for (const target of cases) {
            // test the target against the beginning of name for a match
            if (name.startsWith(target)) {
              hasRelevantTarget = true;
              continue;
            }
          }
        } else {
          // a general modifier
          hasRelevantTarget = true;
        }
        if (hasRelevantTarget) {
          // is the modifier already in the list?
          if (modifiers.includes(mod)) continue;
          // does the modifier have a reference?
          if (entry.formula.includes("@")) {
            // is the reference to a Variable or Pool?
            const regex = /@([\w.\-]+)/gi;
            const reference = (regex.exec(entry.formula))[1];
            const varpool = actor.system.tracked[reference] || actor.system.dynamic[reference];
            if (varpool._id) {
              const vardata = this.items.get(varpool._id);
              if (!modifiers.includes(vardata))
                modifiers.push(vardata);
            }
          }
          modifiers.push(mod);
        }
      }

    }
    const template = "systems/reclaim-the-wild/templates/rollmacro/modifiers.hbs";
    const html = await renderTemplate(template, { actor, rollabledata, modifiers });

    new MyDialog({
      title: "Rollable",
      content: html,
      buttons: {
        back: {
          label: "Back",
          callback: (html) => {
            this.rollables(macroData);
          }
        }
      },
      options: {
        actor: actor,
        macroData: macroData
      }
    }, {
      width: macroData.position.width || 300,
      top: macroData.position.top || 0,
      left: macroData.position.left || 0
    }).render(true);
  }
}
