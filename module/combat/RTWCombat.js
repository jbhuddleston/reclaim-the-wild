export class RTWCombat extends Combat {

  async nextRound() {
    this.combatants.forEach(c => c.actor.resetModVars(true));
    return super.nextRound();
  }
}
