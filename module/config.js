export const rtw = {}

rtw.dataRgx = /[^-(){}\d<>/=*+., DdflorceiMmaxthbsunvpwkxX]/g;

CONFIG.ChatMessage.template = "systems/reclaim-the-wild/templates/chat/chat-message.hbs";

CONFIG.postures = [
    "systems/reclaim-the-wild/icons/postures/standing.png",
    "systems/reclaim-the-wild/icons/postures/sitting.png",
    "systems/reclaim-the-wild/icons/postures/crouching.png",
    "systems/reclaim-the-wild/icons/postures/crawling.png",
    "systems/reclaim-the-wild/icons/postures/kneeling.png",
    "systems/reclaim-the-wild/icons/postures/lyingback.png",
    "systems/reclaim-the-wild/icons/postures/lyingprone.png",
    "systems/reclaim-the-wild/icons/postures/sittingchair.png"
];

CONFIG.sizemods = [
    "systems/reclaim-the-wild/icons/sizemods/smneg1.png",
    "systems/reclaim-the-wild/icons/sizemods/smneg2.png",
    "systems/reclaim-the-wild/icons/sizemods/smneg3.png",
    "systems/reclaim-the-wild/icons/sizemods/smneg4.png",
    "systems/reclaim-the-wild/icons/sizemods/smpos1.png",
    "systems/reclaim-the-wild/icons/sizemods/smpos2.png",
    "systems/reclaim-the-wild/icons/sizemods/smpos3.png",
    "systems/reclaim-the-wild/icons/sizemods/smpos4.png"
];

CONFIG.crippled = [
    "systems/reclaim-the-wild/icons/crippled/crippledleftarm.png",
    "systems/reclaim-the-wild/icons/crippled/crippledlefthand.png",
    "systems/reclaim-the-wild/icons/crippled/crippledleftleg.png",
    "systems/reclaim-the-wild/icons/crippled/crippledleftfoot.png",
    "systems/reclaim-the-wild/icons/crippled/crippledrightarm.png",
    "systems/reclaim-the-wild/icons/crippled/crippledrighthand.png",
    "systems/reclaim-the-wild/icons/crippled/crippledrightleg.png",
    "systems/reclaim-the-wild/icons/crippled/crippledrightfoot.png",
];

CONFIG.statusEffects = [
    { img: 'systems/reclaim-the-wild/icons/postures/standing.png', id: 'standing', name: 'rtw.postures.standing' },
    { img: 'systems/reclaim-the-wild/icons/postures/sitting.png', id: 'sitting', name: 'rtw.postures.sitting' },
    { img: 'systems/reclaim-the-wild/icons/postures/crouching.png', id: 'crouching', name: 'rtw.postures.crouching' },
    { img: 'systems/reclaim-the-wild/icons/postures/crawling.png', id: 'crawling', name: 'rtw.postures.crawling' },
    { img: 'systems/reclaim-the-wild/icons/postures/kneeling.png', id: 'kneeling', name: 'rtw.postures.kneeling' },
    { img: 'systems/reclaim-the-wild/icons/postures/lyingback.png', id: 'lyingback', name: 'rtw.postures.proneb' },
    { img: 'systems/reclaim-the-wild/icons/postures/lyingprone.png', id: 'lyingprone', name: 'rtw.postures.pronef' },
    { img: 'systems/reclaim-the-wild/icons/postures/sittingchair.png', id: 'sittingchair', name: 'rtw.postures.sitting' },
    { img: 'systems/reclaim-the-wild/icons/conditions/shock1.png', id: 'shock1', name: 'rtw.conditions.shock' },
    { img: 'systems/reclaim-the-wild/icons/conditions/shock2.png', id: 'shock2', name: 'rtw.conditions.shock' },
    { img: 'systems/reclaim-the-wild/icons/conditions/shock3.png', id: 'shock3', name: 'rtw.conditions.shock' },
    { img: 'systems/reclaim-the-wild/icons/conditions/shock4.png', id: 'shock4', name: 'rtw.conditions.shock' },
    { img: 'systems/reclaim-the-wild/icons/conditions/reeling.png', id: 'reeling', name: 'rtw.conditions.reeling' },
    { img: 'systems/reclaim-the-wild/icons/conditions/tired.png', id: 'tired', name: 'rtw.conditions.tired' },
    { img: 'systems/reclaim-the-wild/icons/conditions/collapse.png', id: 'collapse', name: 'rtw.conditions.collapse' },
    { img: 'systems/reclaim-the-wild/icons/conditions/unconscious.png', id: 'unconscious', name: 'rtw.conditions.unconscious' },
    { img: 'systems/reclaim-the-wild/icons/conditions/minus1xhp.png', id: 'minushp1', name: 'rtw.conditions.minushp' },
    { img: 'systems/reclaim-the-wild/icons/conditions/minus2xhp.png', id: 'minushp2', name: 'rtw.conditions.minushp' },
    { img: 'systems/reclaim-the-wild/icons/conditions/minus3xhp.png', id: 'minushp3', name: 'rtw.conditions.minushp' },
    { img: 'systems/reclaim-the-wild/icons/conditions/minus4xhp.png', id: 'minushp4', name: 'rtw.conditions.minushp' },
    { img: 'systems/reclaim-the-wild/icons/conditions/stunned.png', id: 'stunned', name: 'rtw.conditions.stunned' },
    { img: 'systems/reclaim-the-wild/icons/conditions/surprised.png', id: 'surprised', name: 'rtw.conditions.surprised' },
    { img: 'systems/reclaim-the-wild/icons/defeated.png', id: 'dead', name: 'rtw.conditions.defeated' },
    { img: 'systems/reclaim-the-wild/icons/blank.png', id: 'none', name: 'rtw.conditions.none' },
    { img: 'systems/reclaim-the-wild/icons/stances/hth.svg', id: 'hth', name: 'rtw.stances.hth' },
    { img: 'systems/reclaim-the-wild/icons/stances/magic.svg', id: 'magic', name: 'rtw.stances.magic' },
    { img: 'systems/reclaim-the-wild/icons/stances/ranged.svg', id: 'ranged', name: 'rtw.stances.ranged' },
    { img: 'systems/reclaim-the-wild/icons/stances/thrown.svg', id: 'thrown', name: 'rtw.stances.thrown' },
    { img: 'systems/reclaim-the-wild/icons/crippled/crippledleftarm.png', id: 'crippledleftarm', name: 'rtw.conditions.crippled' },
    { img: 'systems/reclaim-the-wild/icons/crippled/crippledlefthand.png', id: 'crippledlefthand', name: 'rtw.conditions.crippled' },
    { img: 'systems/reclaim-the-wild/icons/crippled/crippledleftleg.png', id: 'crippledleftleg', name: 'rtw.conditions.crippled' },
    { img: 'systems/reclaim-the-wild/icons/crippled/crippledleftfoot.png', id: 'crippledleftfoot', name: 'rtw.conditions.crippled' },
    { img: 'systems/reclaim-the-wild/icons/crippled/crippledrightarm.png', id: 'crippledrightarm', name: 'rtw.conditions.crippled' },
    { img: 'systems/reclaim-the-wild/icons/crippled/crippledrighthand.png', id: 'crippledrighthand', name: 'rtw.conditions.crippled' },
    { img: 'systems/reclaim-the-wild/icons/crippled/crippledrightleg.png', id: 'crippledrightleg', name: 'rtw.conditions.crippled' },
    { img: 'systems/reclaim-the-wild/icons/crippled/crippledrightfoot.png', id: 'crippledrightfoot', name: 'rtw.conditions.crippled' },
    { img: 'systems/reclaim-the-wild/icons/sizemods/smneg1.png', id: 'smaller1', name: 'rtw.conditions.smaller' },
    { img: 'systems/reclaim-the-wild/icons/sizemods/smneg2.png', id: 'smaller2', name: 'rtw.conditions.smaller' },
    { img: 'systems/reclaim-the-wild/icons/sizemods/smneg3.png', id: 'smaller3', name: 'rtw.conditions.smaller' },
    { img: 'systems/reclaim-the-wild/icons/sizemods/smneg4.png', id: 'smaller4', name: 'rtw.conditions.smaller' },
    { img: 'systems/reclaim-the-wild/icons/sizemods/smpos1.png', id: 'larger1', name: 'rtw.conditions.larger' },
    { img: 'systems/reclaim-the-wild/icons/sizemods/smpos2.png', id: 'larger2', name: 'rtw.conditions.larger' },
    { img: 'systems/reclaim-the-wild/icons/sizemods/smpos3.png', id: 'larger3', name: 'rtw.conditions.larger' },
    { img: 'systems/reclaim-the-wild/icons/sizemods/smpos4.png', id: 'larger4', name: 'rtw.conditions.larger' }
];

CONFIG.controlIcons.defeated = "systems/reclaim-the-wild/icons/defeated.png";

CONFIG.tokenEffects = {
    effectSize: {
        xLarge: 2,
        large: 3,
        medium: 4,
        small: 5
    },
    effectSizeChoices: {
        small: "Small (Default) - 5x5",
        medium: "Medium - 4x4",
        large: "Large - 3x3",
        xLarge: "Extra Large - 2x2"
    }
};

rtw.postures = {
    "standing": "rtw.postures.standing",
    "crouching": "rtw.postures.crouching",
    "kneeling": "rtw.postures.kneeling",
    "crawling": "rtw.postures.crawling",
    "sitting": "rtw.postures.sitting",
    "pronef": "rtw.postures.pronef",
    "proneb": "rtw.postures.proneb"
};
rtw.combat = {
    defaultActionCost: 1,
    defaultShotCost: 3,
    missingInitiative: "You must roll initiative first."
};
rtw.rtw = {
    skillvariables: ['lore-type'],
    spellvariables: ['backwpns', 'backarm', 'backcons', 'backammo', 'backtool'],
    defencevariables: ['head-armor-weight', 'head-armor-rank', 'torso-armor-weight', 'torso-armor-rank', 'leg-armor-weight', 'leg-armor-rank'],
    attackvariables: ['attack-type', 'damage'],
    selectOptions: {
        defence_category: [
            { value: "dodge", label: "rtw.item.defence.dodge" },
            { value: "parry", label: "rtw.item.defence.parry" },
            { value: "block", label: "rtw.item.defence.block" }
        ],
        feature_category: [
            { value: "advantage", label: "rtw.item.feature.advantage" },
            { value: "disadvantage", label: "rtw.item.feature.disadvantage" },
            { value: "perk", label: "rtw.item.feature.perk" },
            { value: "quirk", label: "rtw.item.feature.quirk" }
        ],
        ingredient_type: [
            { value: "food", label: "rtw.item.ingredient.food" },
            { value: "critter", label: "rtw.item.ingredient.critter" }
        ],
        modifier_category: [
            { value: "attack", label: "rtw.item.modifier.attack" },
            { value: "defence", label: "rtw.item.modifier.defence" },
            { value: "skill", label: "rtw.item.rollable.skill" },
            { value: "spell", label: "rtw.item.rollable.spell" },
            { value: "reaction", label: "rtw.item.modifier.reaction" },
            { value: "check", label: "rtw.item.rollable.check" },
            { value: "damage", label: "rtw.item.modifier.damage" },
            { value: "primary", label: "rtw.item.modifier.primary" }
        ],
        points_stat: [
            { value: "hp", label: "rtw.item.points.hp" },
            { value: "mp", label: "rtw.item.points.mp" },
            { value: "sp", label: "rtw.item.points.sp" }
        ],
        points_type: [
            { value: "damage", label: "rtw.item.points.damage" },
            { value: "spend", label: "rtw.item.points.spend" },
            { value: "bind", label: "rtw.item.points.bind" },
            { value: "burn", label: "rtw.item.points.burn" },
            { value: "temp", label: "rtw.item.points.temp" }
        ],
        rollable_category: [
            { value: "check", label: "rtw.item.rollable.check" },
            { value: "skill", label: "rtw.item.rollable.skill" },
            { value: "spell", label: "rtw.item.rollable.spell" },
            { value: "technique", label: "rtw.item.rollable.technique" },
            { value: "rms", label: "rtw.item.rollable.rms" }
        ],
        weapon_location: [
            { value: "hand", label: "rtw.item.weapon.hand" },
            { value: "belt", label: "rtw.item.weapon.belt" },
            { value: "back", label: "rtw.item.weapon.back" }
        ]
    }
};
