/**
* Organise some data before rendering the sheet
*/
Hooks.on("getBaseActorSheetHeaderButtons", (sheet, buttonarray) => {
  if (CONFIG.rtw.testMode) { console.debug("getBaseActorSheetHeaderButtons\n", [sheet, buttonarray]); }
  return;
  // This happens before the call to render the sheet
});

/**
* Organise some data before rendering the sheet
*/
Hooks.on("renderBaseActorSheet", (sheet, html, data) => {
  const actor = sheet.actor;
  if (CONFIG.rtw.testMode) { console.debug("renderBaseActorSheet\n", [sheet, html, data, actor.system]); }
  return;
});

/**
* Organise some data before closing the sheet
*
* Reset temporary modifiers and variables
*/
Hooks.on("closeActorSheet", (sheet, html) => {
  const actor = sheet.actor;
  if (CONFIG.rtw.testMode) { console.debug("closeActorSheet\n", [sheet, html, actor.system]); }
  if (CONFIG.rtw.resetTemporaryItemsOnSheetClose) actor.resetModVars(true);
  return;
});

/**
 * Specific Tailoring of actor types on creation
 * 
 * Preparing to fake actor subclasses and need to make all actor templates the same
 */
Hooks.on('preCreateActor', (document, data, options, userId) => {
  if (CONFIG.rtw.testMode) console.debug("preCreateActor:\n", [document, data, options, userId]);
  // if the actor has an image, it already exists. No reason to be doing anything to it here yet.
  if (data.img) return;

  const newdata = {};

  // set the basic features according to system requirements
  switch (data.type) {
    case "Hero": {
      newdata.bm = {
        step: 30
      }
      break;
    }
  }
  data.system = newdata;
  document.updateSource(data);
});


/**
 * Specific Tailoring of actor types on creation
 * 
 * Preparing to fake actor subclasses and need to make all actor templates the same
 */
Hooks.on('preUpdateActor', (document, data, options, userId) => {
  if (CONFIG.rtw.testMode) console.debug("preUpdateActor:\n", [document, data, options, userId]);

  // if diff is false it is probably a data import otherwise return
  if (options.diff) return;

  try {
    console.debug(ImagePopout.getImageSize(data.img));
    return;
  } catch (err) {
    console.error(err);
  }

  // if you get here, there is no image or they are in a place we cannot be sure to reach
  data.img = "icons/svg/mystery-man-black.svg";
  data.token.img = "icons/svg/mystery-man-black.svg";

  document.updateSource(data);
});
