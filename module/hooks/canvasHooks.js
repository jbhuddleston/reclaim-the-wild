Hooks.on('dropCanvasData', (canvas, data) => {
    // is there a token under the drop point?
    for (let token of canvas.tokens.ownedTokens) {
        if (token.x > data.x) continue;
        if (token.y > data.y) continue;
        if (token.x + token.w < data.x) continue;
        if (token.y + token.h < data.y) continue;

        return token.actor.sheet._onTokenDrop(data);
    }

});
