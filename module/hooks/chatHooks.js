Hooks.on('preCreateChatMessage', (message, options, userid) => {
    if (message.flags.core?.initiativeRoll) {
        return false;
    }
});

/**
 * add listeners to chat messages in the log
 */
 Hooks.on("renderChatLog", (log, html, data) => {

    html.on('click', '.open-journal', ev => {
      ev.preventDefault();
  
      const journal = ev.currentTarget.dataset.table;
      try {
        Journal._showEntry(game.journal.getName(journal).uuid, "text", true);
      } catch(err) {
        if (journal) ui.notifications.error(`There is no matching Journal Entry for ${journal}. Perhaps you have yet to import it from the compendium?`);
      }
    });
  
  });
  
  Hooks.on("renderChatMessage", async (app, html, msg) => {
    const actor = game.actors.get(msg.message.speaker.actor) || null;

    // process the rollable item
    html.on('click', '.rollable', ev => {
      actor?.sheet._onRoll(ev);
    });

    html.find(".critresult").each(function () {
      let exp = $(this)[0]
      exp.setAttribute("draggable", true)
      exp.addEventListener('dragstart', ev => {
        let dataTransfer = {
          type: $(exp).attr("data-type"),
          message: $(exp).attr("data-message"),
          woundtitle: $(exp).attr("data-woundtitle"),
          condition: $(exp).attr("data-condition"),
          hits: $(exp).attr("data-hits"),
          bleed: $(exp).attr("data-bleed"),
          action: $(exp).attr("data-action"),
          effect: $(exp).attr("data-effect")
        }
        ev.dataTransfer.setData("text/plain", JSON.stringify(dataTransfer));
      })
    });

    html.find(".damageresult").each(function () {
      let exp = $(this)[0]
      exp.setAttribute("draggable", true)
      exp.addEventListener('dragstart', ev => {
        let dataTransfer = {
          type: $(exp).attr("data-type"),
          hits: $(exp).attr("data-hits")
        }
        ev.dataTransfer.setData("text/plain", JSON.stringify(dataTransfer));
      })
    });

    html.find(".critseverity").each(function () {
      let exp = $(this)[0]
      exp.setAttribute("draggable", true)
      exp.addEventListener('dragstart', ev => {
        let dataTransfer = {
          type: $(exp).attr("data-type"),
          crit: $(exp).attr("data-crit")
        }
        ev.dataTransfer.setData("text/plain", JSON.stringify(dataTransfer));
      })
    });
    
  });