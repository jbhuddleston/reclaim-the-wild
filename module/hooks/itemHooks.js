Hooks.on('createItem', (document, options, userId) => {
  if (CONFIG.rtw.testMode) console.debug("createItem:\n", [document, options, userId]);
  const docdata = document.system;

  // items dragged from the sidebar will lose their moddedformula if they have one so calculate it again
  switch (document.type) {
    case "Weapon":
    case "Defence":
    case "Rollable": {
      if (CONFIG.rtw.testMode) console.debug("Item Data:\n", docdata);
      if (docdata.formula) { // we will recalculate the value if the formula does not contain an @dependency
        let formula = docdata.formula;
        if (Number.isNumeric(formula)) {
          // the formula is a number
          docdata.value = Number(formula);
        } else if (formula.includes("#") || formula.includes("@")) {
          // the formula has ranks or a reference and will be processed in prepareDerivedData
        } else {
          // the formula should be a valid dice expression so has no value
          docdata.value = 0;
          docdata.moddedvalue = 0;
          docdata.moddedformula = formula;
        }
      }
      break;
    }
    case "Modifier": {
      if (docdata.alwaysOn) docdata.inEffect = true;
      break;
    }
  }
});

Hooks.on('preCreateItem', (document, data, options, userId) => {
  if (CONFIG.rtw.testMode) console.debug("preCreateItem:\n", [document, data, options, userId]);

  if (data == undefined) {
    // the item is being created from the item sidebar
    data = document.system;
  }

  if (!data.group) { // initialise group field to match item category or type
    data.group = data.category || document.type;
  }
  if (!data.img) {
    data.img = "icons/svg/mystery-man-black.svg";
  }
  document.updateSource(data);
});
