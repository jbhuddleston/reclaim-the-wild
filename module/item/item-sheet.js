/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class BaseItemSheet extends ItemSheet {

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["reclaim-the-wild", "sheet", "item"],
      width: 450,
      height: 450,
    });
  }

  /** @override */
  get template() {
    const path = "systems/reclaim-the-wild/templates/item";
    // Return a single sheet for all item types.
    // return `${path}/item-sheet.hbs`;
    // Alternatively, you could use the following return statement to do a
    // unique item sheet by type, like `weapon-sheet.hbs`.

    return `${path}/${this.item.type.toLowerCase()}-sheet.hbs`;
  }

  /* -------------------------------------------- */

  /** @override */
  async getData(options) {
    const context = await super.getData(options);
    const item = context.item;
    context.system = item.system;
    context.config = CONFIG.rtw;
    return context;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Roll handlers, click handlers, etc. would go here.
    html.find('.entry-create').click(this._onEntryCreate.bind(this));
    html.find('.entry-delete').click(this._onEntryDelete.bind(this));
    html.find('.entry-clone').click(this._onEntryClone.bind(this));

    const handler = (ev) => this._onDragStart(ev);
    html.find(".draggable").each((i, li) => {
      li.setAttribute("draggable", true);
      li.addEventListener("dragstart", handler, true);
    });
  }

  /**
   * Handle the Entry Create click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onEntryCreate(event) {
    const itemdata = this.item.system;
    const entries = itemdata.entries;
    entries.push(entries[0]);
    entries[0] = { value: 0, formula: "", category: "", targets: "" };
    this.item.update({ 'system': itemdata });
  }

  /**
   * Handle the Entry Delete click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onEntryDelete(event) {
    const index = event.currentTarget.closest(".item").dataset.id;
    const itemdata = this.item.system;
    itemdata.entries.splice(index, 1);
    this.item.update({ 'system': itemdata });
  }

  /**
   * Handle the Entry Clone click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onEntryClone(event) {
    const index = event.currentTarget.closest(".item").dataset.id;
    const itemdata = this.item.system;
    const entries = itemdata.entries;
    entries.push(entries[index]);
    this.item.update({ 'system': itemdata });
  }

  /**
   * Replace referenced data attributes in the formula with the syntax `@attr` with the corresponding key from
   * the provided `data` object.
   * @param {String} formula    The original formula within which to replace
   * @private
   */
  _replaceData(formula) {
    let dataRgx = /@([\w.\-]+)/gi;
    let findTerms = (match, term) => {
      let value = this.actor?.system.dynamic[term]?.value || this.actor?.system.tracked[term]?.value;
      return (value) ? String(value).trim() : "0";
    };
    return { value: formula.replace(dataRgx, findTerms).replace(/(min)|(max)|(floor)|(ceil)|(round)|(abs)|(pow)/g, "Math.$&") };
  }
}
export class PoolItemSheet extends BaseItemSheet {

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Roll handlers, click handlers, etc. would go here.
    html.find('.pool').change(this._onInputChange.bind(this));
  }

  /**
   * Handle the Item Input Change Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onInputChange(event) { // only for pools as they must update tokens
    if (CONFIG.rtw.testMode) console.debug("entering _onInputChange()\n", event);

    const target = event.currentTarget;
    // get the name of the changed element
    const dataname = target.name.split(".")[1];
    // get the new value
    let value = target.value;
    // is this the value attribute, isBar is true
    let isBar = (dataname == "value");

    this.actor?.modifyTokenAttribute(
      `tracked.${this.item.system.abbr.toLowerCase()}${isBar ? '' : `.${dataname}`}`,
      value,
      false, isBar
    );
  }
}

export class ModifierItemSheet extends BaseItemSheet {

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["reclaim-the-wild", "sheet", "item", "modifier"],
      width: 600,
      height: 400,
      dragDrop: [
        { dragSelector: '.draggable', dropSelector: '.modentry' },
      ],
    });
  }

  async _onDragStart(event) {
    const dataset = event.currentTarget.dataset;

    // Create drag data
    let dragData = {
      itemId: dataset.itemId,
      index: dataset.index,
      type: "modentry",
      value: dataset.value,
      category: dataset.category,
      formula: dataset.formula,
      targets: dataset.targets
    }

    if (!dragData) return;

    // Set data transfer
    await event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
  }

  async _onDrop(event) {
    // Try to extract the data
    let transferData;
    try {
      transferData = JSON.parse(event.dataTransfer.getData("text/plain"));
    } catch (err) {
      return false;
    }
    if (CONFIG.rtw.testMode) console.debug(`processing ${transferData.type}\n`, transferData);

    const targetindex = Number(event.currentTarget.dataset.id);
    const sourceindex = Number(transferData.index);
    const sourceItemId = transferData.itemId;
    let formula;
    let targets;
    let value;
    let category;
    switch (transferData.type) {
      case "varentry": {
        value = transferData.value;
        formula = transferData.formula;
        category = "attack";
        targets = transferData.label;
        break;
      }
      case "modentry": {
        value = transferData.value;
        formula = transferData.formula;
        category = transferData.category;
        targets = transferData.targets;
        break;
      }
    }
    const entry = {
      value: value,
      formula: formula,
      category: category,
      targets: targets
    };
    const itemdata = this.item.system;
    const entries = itemdata.entries;
    let newsource = sourceindex;

    if (this.item.id != sourceItemId) {
      // inserting an entry
      entries.push(entry);
      newsource = entries.length-1;
    }
    // re-ordering entries
    if (newsource > targetindex) {
      // moving up
      for (let i = newsource - 1; i != targetindex; i--) {
        [entries[i], entries[newsource]] = [entries[newsource], entries[i]];
        newsource--;
      }
    } else {
      // moving down
      for (let i = newsource + 1; i != targetindex; i++) {
        [entries[i], entries[newsource]] = [entries[newsource], entries[i]];
        newsource++;
      }
    }

    let data = {
      chartype: itemdata.chartype,
      inEffect: itemdata.inEffect,
      notes: itemdata.notes,
      entries: entries
    }
    this.item.update({ 'system': data });
  }
}

export class VariableItemSheet extends BaseItemSheet {

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["reclaim-the-wild", "sheet", "item", "variable"],
      width: 300,
      height: 400,
      dragDrop: [
        { dragSelector: '.draggable', dropSelector: '.varentry' },
      ],
    });
  }

  async _onDragStart(event) {
    const dataset = event.currentTarget.dataset;

    // Create drag data
    let dragData = {
      itemId: dataset.itemId,
      index: dataset.index,
      type: "varentry",
      value: dataset.value,
      formula: dataset.formula,
      label: dataset.label
    }

    if (!dragData) return;

    // Set data transfer
    await event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
  }

  async _onDrop(event) {
    // Try to extract the data
    let transferData;
    try {
      transferData = JSON.parse(event.dataTransfer.getData("text/plain"));
    } catch (err) {
      return false;
    }
    if (CONFIG.rtw.testMode) console.debug(`processing ${transferData.type}\n`, transferData);

    const targetindex = Number(event.currentTarget.dataset.id);
    const sourceindex = Number(transferData.index);
    const sourceItemId = transferData.itemId;
    let formula;
    let label;
    let value;
    switch (transferData.type) {
      case "varentry": {
        value = transferData.value;
        formula = transferData.formula;
        label = transferData.label;
        break;
      }
      case "modentry": {
        value = transferData.value;
        formula = transferData.formula;
        label = transferData.targets;
        break;
      }
    }
    const entry = {
      value: value,
      formula: formula,
      label: label
    };
    const itemdata = this.item.system;
    const entries = itemdata.entries;
    let newsource = sourceindex;

    if (this.item.id != sourceItemId) {
      // inserting an entry
      entries.push(entry);
      newsource = entries.length-1;
    }
    // re-ordering entries
    if (newsource > targetindex) {
      // moving up
      for (let i = newsource - 1; i != targetindex; i--) {
        [entries[i], entries[newsource]] = [entries[newsource], entries[i]];
        newsource--;
      }
    } else {
      // moving down
      for (let i = newsource + 1; i != targetindex; i++) {
        [entries[i], entries[newsource]] = [entries[newsource], entries[i]];
        newsource++;
      }
    }

    let data = {
      chartype: itemdata.chartype,
      notes: itemdata.notes,
      entries: entries
    }
    this.item.update({ 'system': data });
  }
}
