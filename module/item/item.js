/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
export class BaseItem extends Item {
  /**
   * Augment the basic Item data model with additional dynamic data.
   */
  prepareDerivedData() {
    const itemdata = this.system;
    switch (this.type) {
      case "Variable": {
        if (!Array.isArray(itemdata.entries)) {
          itemdata.entries = Object.values(itemdata.entries);
        }
        const entries = itemdata.entries;
        let notfound = true;
        for (const entry of entries) {
          if (entry.label != itemdata.label && itemdata.label != "") continue;
          notfound = false;
          if (Number.isNumeric(entry.formula)) {
            entry.value = Number(entry.formula);
          } else {
            entry.value = 0;
          }
          itemdata.moddedformula = itemdata.formula = entry.formula;
          itemdata.value = itemdata.moddedvalue = entry.value;
        }
        if (notfound && entries.length > 1) {
          itemdata.formula = entries[1].formula;
          itemdata.label = entries[1].label;
        }
        break;
      }
      case "Modifier": {
        if (itemdata.alwaysOn) itemdata.inEffect = true;
        itemdata.attack = false;
        itemdata.damage = false;
        itemdata.defence = false;
        itemdata.reaction = false;
        itemdata.skill = false;
        itemdata.spell = false;
        itemdata.check = false;
        itemdata.primary = false;
        if (!Array.isArray(itemdata.entries)) {
          itemdata.entries = Object.values(itemdata.entries);
        }
        const entries = itemdata.entries;
        for (let i = 1; i < entries.length; i++) { //ignore category changes to [0]
          switch (entries[i].category) {
            case "attack": itemdata.attack = true; break;
            case "damage": itemdata.damage = true; break;
            case "defence": itemdata.defence = true; break;
            case "reaction": itemdata.reaction = true; break;
            case "skill": itemdata.skill = true; break;
            case "spell": itemdata.spell = true; break;
            case "check": itemdata.check = true; break;
            case "primary": itemdata.primary = true; break;
          }
        }
        break;
      }
      case "Defence":
      case "Rollable": {
        if (Number.isNumeric(itemdata.formula)) {
          // the formula is a number
          itemdata.value = Number(itemdata.formula);
        } else if (itemdata.formula.includes("#") || itemdata.formula.includes("@")) {
          // the formula has ranks or a reference and will be processed in prepareDerivedData
        } else {
          // the formula should be a valid dice expression so has no value
          itemdata.value = 0;
          itemdata.moddedvalue = 0;
          itemdata.moddedformula = itemdata.formula;
        }
        break;
      }
      case "Pool": {
        if (Number.isNumeric(itemdata.maxForm)) {
          // the formula is a number
          itemdata.max = Number(itemdata.maxForm);
        } else if (itemdata.maxForm.includes("@")) {
          // the formula has ranks or a reference and will be processed in prepareDerivedData
        } else {
          // the formula should be a valid dice expression so has no value
          itemdata.max = 0;
        }
        if (Number.isNumeric(itemdata.minForm)) {
          // the formula is a number
          itemdata.min = Number(itemdata.minForm);
        } else if (itemdata.minForm.includes("@")) {
          // the formula has ranks or a reference and will be processed in prepareDerivedData
        } else {
          // the formula should be a valid dice expression so has no value
          itemdata.min = 0;
        }
        itemdata.value = Math.clamp(itemdata.min, itemdata.value, itemdata.max);
        break;
      }
      default: {
        // do nothing yet
      }
    }
  }
}
