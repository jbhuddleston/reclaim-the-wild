/**
 * Perform a system migration for the entire World, applying migrations for Actors, Items, and Compendium packs
 * @return {Promise}      A Promise which resolves once the migration is completed
 */
export const migrateWorld = async function () {
  ui.notifications.notify(`Beginning Migration to reclaim-the-wild ${game.system.version}`, { permanent: true });

  // Migrate World Actors
  for (let a of game.actors.contents) {
    try {
      const updateData = await migrateActorData(a.data);
      if (!foundry.utils.isEmpty(updateData)) {
        console.debug(`Migrating Actor ${a.name}`);
        await a.update(updateData, { enforceTypes: false });
      }
    } catch (err) {
      err.message = `Failed migration for Actor ${a.name}: ${err.message}`;
      console.error(err);
    }
  }

  // Migrate World Items
  for (let i of game.items.contents) {
    try {
      const updateData = await migrateItemData(i.toObject());
      if (!foundry.utils.isEmpty(updateData)) {
        console.debug(`Migrating Item ${i.name}`);
        await i.update(updateData, { enforceTypes: false });
      }
    } catch (err) {
      err.message = `Failed migration for Item ${i.name}: ${err.message}`;
      console.error(err);
    }
  }

  // Migrate Actor Override Tokens
  for (let s of game.scenes.contents) {
    try {
      const updateData = await migrateSceneData(s.data);
      if (!foundry.utils.isEmpty(updateData)) {
        console.debug(`Migrating Scene entity ${s.name}`);
        await s.update(updateData, { enforceTypes: false });
        // If we do not do this, then synthetic token actors remain in cache
        // with the un-updated actordata.
        s.tokens.contents.forEach(t => t._actor = null);
      }
    } catch (err) {
      err.message = `Failed migration for Scene ${s.name}: ${err.message}`;
      console.error(err);
    }
  }

  // Migrate World Compendium Packs
  for (let p of game.packs) {
    if (p.metadata.package !== "world") continue; // comment out to migrate system packs TODO: there is something wrong with system pack migration
    if (!["Actor", "Item", "Scene"].includes(p.documentName)) continue;
    await migrateCompendium(p);
  }

  game.settings.set("reclaim-the-wild", "systemMigrationVersion", game.system.version);
  ui.notifications.notify(`Migration to RECLAIM-THE-WILD ${game.system.version} Finished`, { permanent: true });
}

/**
 * Apply migration rules to all Documents within a single Compendium pack
 * @param pack
 * @return {Promise}
 */
export const migrateCompendium = async function (pack) {
  const entity = pack.documentName;
  if (!["Actor", "Item", "Scene"].includes(entity)) return;

  // Unlock the pack for editing
  const wasLocked = pack.locked;
  await pack.configure({ locked: false });

  // Begin by requesting server-side data model migration and get the migrated content
  await pack.migrate();
  const documents = await pack.getDocuments();

  // Iterate over compendium entries - applying fine-tuned migration functions
  for (let doc of documents) {
    let updateData = {};
    try {
      switch (entity) {
        case "Actor":
          updateData = migrateActorData(doc.data);
          break;
        case "Item":
          updateData = migrateItemData(doc.toObject());
          break;
        case "Scene":
          updateData = migrateSceneData(doc.data);
          break;
      }

      // Save the entry, if data was changed
      if (foundry.utils.isEmpty(updateData)) continue;
      await doc.update(updateData);
      console.debug(`Migrated ${entity} entity ${doc.name} in Compendium ${pack.collection}`);
    }
    catch (err) {    // Handle migration failures
      err.message = `Failed migration for entity ${doc.name} in pack ${pack.collection}: ${err.message}`;
      console.error(err);
    }
  }

  // Apply the original locked status for the pack
  await pack.configure({ locked: wasLocked });
  console.debug(`Migrated all ${entity} entities from Compendium ${pack.collection}`);
};

/**
 * Migrate a single Actor entity to incorporate latest data model changes
 * Return an Object of updateData to be applied
 * @param {object} actor    The actor data object to update
 * @return {Object}         The updateData to apply
 */
export const migrateActorData = async function (actor) {
  const updateData = {};

  // Actor Data Updates go here
  if (actor.system) {
    //if (actor.type == "CharacterVsD") await _migrateActorDeclaration(actor, updateData);
  }

  // Migrate Owned Items
  if (!actor.items) return updateData;
  const items = actor.items.reduce((arr, i) => {
    // Migrate the Owned Item
    const itemdata = i instanceof CONFIG.Item.documentClass ? i.toObject() : i;
    let itemUpdate = migrateItemData(itemdata);

    // Update the Owned Item
    if (!foundry.utils.isEmpty(itemUpdate)) {
      itemUpdate._id = itemdata._id;
      arr.push(foundry.utils.expandObject(itemUpdate));
    }

    return arr;
  }, []);
  if (items.length > 0) updateData.items = items;
  return updateData;
}

/**
 * Migrate a single Item entity to incorporate latest data model changes
 *
 * @param {object} item  Item data to migrate
 * @return {object}      The updateData to apply
 */
export const migrateItemData = function (item) {
  const updateData = {};
  switch (item.type) {
    case "Pool": _migratePools(item, updateData); break;
  }
  return updateData;
}

/**
 * Migrate a single Scene entity to incorporate changes to the data model of its actor data overrides
 * Return an Object of updateData to be applied
 * @param {Object} scene  The Scene data to Update
 * @return {Object}       The updateData to apply
 */
export const migrateSceneData = function (scene) {
  const tokens = scene.tokens.map(token => {
    const t = token.toJSON();
    if (!t.actorId || t.actorLink) {
      t.delta = {};
    }
    else if (!game.actors.has(t.actorId)) {
      t.actorId = null;
      t.delta = {};
    }
    else if (!t.actorLink) {
      const delta = foundry.utils.duplicate(t.delta);
      delta.type = token.actor?.type;
      const update = migrateActorData(delta);
      ['items', 'effects'].forEach(embeddedName => {
        if (!update[embeddedName]?.length) return;
        const updates = new Map(update[embeddedName].map(u => [u._id, u]));
        t.delta[embeddedName].forEach(original => {
          const update = updates.get(original._id);
          if (update) foundry.utils.mergeObject(original, update);
        });
        delete update[embeddedName];
      });

      foundry.utils.mergeObject(t.delta, update);
    }
    return t;
  });
  return { tokens };
};

/**
 * Convert Pools to use formulae for Max and Min
 *
 * @param {object} item        Item data to migrate
 * @param {object} updateData  Existing update to expand upon
 * @return {object}            The updateData to apply
 * @private
 */
function _migratePools(item, updateData) {
  switch (item.system.minForm) {
    case "":
    case "0":
      updateData[`system.minForm`] = `${item.system.min}`;
      console.log(`${item.system.abbr}: min: ${item.system.min}, minF: ${item.system.minForm}, value: ${item.system.value}, max: ${item.system.max}, maxF: ${item.system.maxForm}`);
      break;
  }
  switch (item.system.maxForm) {
    case "":
    case "0":
      if (item.system.value > item.system.max) {
        updateData[`system.max`] = `${item.system.value}`;
        updateData[`system.maxForm`] = `${item.system.value}`;
      } else {
        updateData[`system.maxForm`] = `${item.system.max}`;
      }
      console.log(`${item.system.abbr}: min: ${item.system.min}, minF: ${item.system.minForm}, value: ${item.system.value}, max: ${item.system.max}, maxF: ${item.system.maxForm}`);
      break;
  }
  return updateData;
}


export class Refresh {

  static async refreshWorld() {
    ui.notifications.notify(`Beginning System Refresh`);
    for (let i of game.items.contents) {
      await this.refreshItemData(i, foundry.utils.duplicate(i.data), null);
    }

    for (let a of game.actors.contents) {
      if (CONFIG.rtw.testMode) console.debug(a.name);
      a.update(await this.refreshActorData(a));
    }

    for (let p of game.packs) {
      if (p.documentName == "Item" && p.metadata.package == "world")
        p.getContent().then(async (items) => {
          items.forEach(async (i) => {
            await this.refreshItemData(i, foundry.utils.duplicate(i.data), null);
          })
        })

      if (p.documentName == "Actor" && p.metadata.package == "world") {
        p.getContent().then(async (actors) => {
          actors.forEach(async (a) => {
            p.updateEntity(await this.refreshActorData(a))
          })
        })
      }
    }
    ui.notifications.notify(`Refresh of RECLAIM-THE-WILD Finished`);
  }

  static async refreshActorData(actor) {
    for (let i of actor.items.contents) {
      await this.refreshItemData(i, foundry.utils.duplicate(i.data), actor);
    }
    return actor.system
  }

  static async refreshItemData(item, itemdata, actor) {
    // check each entry in a Variable to make sure that the value matches the formula.
    switch (item.type) {
      case "Variable": {
        let entries = itemdata.entries;
        for (let i = 1; i < entries.length; i++) {
          if (entries[i].value != Number(entries[i].formula)) {
            if (Number.isNumeric(entries[i].formula)) {
              console.debug(`${entries[i].value} being corrected to ${entries[i].formula} in ${entries[i].label} of ${item.name} in ${actor?.name}`);
              entries[i].value = Number(entries[i].formula);
            }
          }
        }
        break;
      }
      case "Pool": {
        break;
      }
    }
    return await item.update(itemdata);
  }
}

/**
 * Ready hook loads tables, and override's foundry's entity link functions to provide extension to pseudo entities
 */
Hooks.once("ready", function () {

  if (CONFIG.rtw.testMode) console.debug("Starting Ready");

  // Determine whether a system migration is required and feasible
  if (!game.user.isGM) return;
  const currentVersion = game.settings.get("reclaim-the-wild", "systemMigrationVersion");
  const NEEDS_MIGRATION_VERSION = 1.00;
  const COMPATIBLE_MIGRATION_VERSION = 1.00;
  const totalDocuments = game.actors.size + game.scenes.size + game.items.size;
  if (!currentVersion && totalDocuments === 0) return game.settings.set("reclaim-the-wild", "systemMigrationVersion", game.system.version);
  const needsMigration = !currentVersion || foundry.utils.isNewerVersion(NEEDS_MIGRATION_VERSION, currentVersion);
  if (!needsMigration) return;

  // Perform the migration
  if (currentVersion && foundry.utils.isNewerVersion(COMPATIBLE_MIGRATION_VERSION, currentVersion)) {
    const warning = `Your system data is from too old a Foundry version and cannot be reliably migrated to the latest version. The process will be attempted, but errors may occur.`;
    ui.notifications.error(warning, { permanent: true });
  }
  return migrateWorld();
});
