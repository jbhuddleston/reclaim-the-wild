// Import Modules
import { BaseActor } from "./module/actor/baseActor.js";
import { ActorRtWSheet } from "./module/actor/actorRtW-sheet.js";
import { BaseItem } from "./module/item/item.js";
import { BaseItemSheet, PoolItemSheet, ModifierItemSheet, VariableItemSheet } from "./module/item/item-sheet.js";
import { rtw } from "./module/config.js";
import { RTWTokenDocument, RTWToken } from "./module/token.js";
import { RTWCombat } from "./module/combat/RTWCombat.js";
import { TokenEffects } from "./module/token.js";

// Pre-load templates
async function preloadHandlebarsTemplates() {
  const templatePaths = [
    "systems/reclaim-the-wild/templates/partials/gmod.hbs",
    "systems/reclaim-the-wild/templates/partials/notes.hbs",
    "systems/reclaim-the-wild/templates/partials/owneditems.hbs",
    "systems/reclaim-the-wild/templates/partials/scripts.hbs",
    "systems/reclaim-the-wild/templates/partials/sectiontitles.hbs"
  ];
  return loadTemplates(templatePaths);
};

Roll.CHAT_TEMPLATE = "systems/reclaim-the-wild/templates/dice/roll.hbs";
Roll.TOOLTIP_TEMPLATE = "systems/reclaim-the-wild/templates/dice/tooltip.hbs";

Hooks.once('init', () => {

  CONFIG.rtw = rtw;

  // Define custom Entity classes
  CONFIG.Actor.documentClass = BaseActor;
  CONFIG.Item.documentClass = BaseItem;
  CONFIG.Token.documentClass = RTWTokenDocument;
  CONFIG.Token.objectClass = RTWToken;

  // Which actor in this browser most recently made a roll
  game.settings.register("reclaim-the-wild", "currentActor", {
    name: "Current Actor Name",
    hint: "Which actor most recently made a roll on this browser?",
    scope: "client",
    config: false,
    default: "null",
    type: String
  });

  game.settings.register("reclaim-the-wild", "systemMigrationVersion", {
    name: "System Migration Version",
    scope: "world",
    config: true,
    type: Number,
    default: 1.00
  });

  CONFIG.Combat.initiative = {
    formula: "2d6 + @dynamic.initiative.moddedvalue",
    decimals: 0
  }

  if (CONFIG.rtw.rtw) {
    // Register Defence Variables
    game.settings.register("reclaim-the-wild", "defenceVariables", {
      name: "SETTINGS.DefenceVariables",
      hint: "SETTINGS.DefenceVariablesHint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.rtw.rtw.defencevariables.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.rtw.rtw.defencevariables = game.settings.get("reclaim-the-wild", "defenceVariables").split(",").map(word => word.trim());

    // Register Attack Variables
    game.settings.register("reclaim-the-wild", "attackVariables", {
      name: "SETTINGS.AttackVariables",
      hint: "SETTINGS.AttackVariablesHint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.rtw.rtw.attackvariables.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.rtw.rtw.attackvariables = game.settings.get("reclaim-the-wild", "attackVariables").split(",").map(word => word.trim());

    // Register Skill Variables
    game.settings.register("reclaim-the-wild", "skillVariables", {
      name: "SETTINGS.SkillVariables",
      hint: "SETTINGS.SkillVariablesHint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.rtw.rtw.skillvariables.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.rtw.rtw.skillvariables = game.settings.get("reclaim-the-wild", "skillVariables").split(",").map(word => word.trim());

    // Register Spell Variables
    game.settings.register("reclaim-the-wild", "spellVariables", {
      name: "SETTINGS.SpellVariables",
      hint: "SETTINGS.SpellVariablesHint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.rtw.rtw.spellvariables.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.rtw.rtw.spellvariables = game.settings.get("reclaim-the-wild", "spellVariables").split(",").map(word => word.trim());
  };

  // Register Show Test Data
  game.settings.register("reclaim-the-wild", "showTestData", {
    name: "SETTINGS.ShowTestData",
    hint: "SETTINGS.ShowTestDataHint",
    scope: "world",
    config: true,
    default: false,
    requiresReload: true,
    type: Boolean
  });
  CONFIG.rtw.testMode = game.settings.get("reclaim-the-wild", "showTestData");

  // Register Show Hooks Data
  game.settings.register("reclaim-the-wild", "showHooks", {
    name: "SETTINGS.ShowHooks",
    hint: "SETTINGS.ShowHooksHint",
    scope: "world",
    config: true,
    default: false,
    requiresReload: true,
    type: Boolean
  });
  CONFIG.debug.hooks = game.settings.get("reclaim-the-wild", "showHooks");

  // Register Resetting of Temporary Items on sheet closing
  game.settings.register("reclaim-the-wild", "resetTemps", {
    name: "SETTINGS.ResetTemps",
    hint: "SETTINGS.ResetTempsHint",
    scope: "client",
    config: true,
    default: false,
    requiresReload: true,
    type: Boolean
  });
  CONFIG.rtw.resetTemporaryItemsOnSheetClose = game.settings.get("reclaim-the-wild", "resetTemps");

  /*
  game.settings.register("reclaim-the-wild", "effectSize", {
    name: "SETTINGS.TokenEffectSize",
    hint: "SETTINGS.TokenEffectSizeHint",
    default: CONFIG.tokenEffects.effectSizeChoices.small,
    scope: "client",
    type: String,
    choices: CONFIG.tokenEffects.effectSizeChoices,
    config: true,
    onChange: s => {
      TokenEffects.patchCore();
      canvas.draw();
    }
  });
  */

  // ================ end of system settings
  // use the Foundry Default settings
  CONFIG.Combat.documentClass = RTWCombat;

  // Change the thickness of the border around Objects. Default = 4
  CONFIG.Canvas.objectBorderThickness = 12;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("reclaim-the-wild", ActorRtWSheet, {
    types: ["Hero"],
    makeDefault: true,
    label: "Reclaim the Wild Character"
  });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("reclaim-the-wild", BaseItemSheet, {
    types: ["Weapon", "Container", "Rollable", "Feature", "Defence", "Equipment", "Hit-Location", "Points", "Token-of-Heroism", "Ingredient"],
    makeDefault: true,
    label: "Items"
  });
  Items.registerSheet("reclaim-the-wild", PoolItemSheet, {
    types: ["Pool"],
    makeDefault: true,
    label: "Pool Item"
  });
  Items.registerSheet("reclaim-the-wild", ModifierItemSheet, {
    types: ["Modifier"],
    makeDefault: true,
    label: "Modifier Item"
  });
  Items.registerSheet("reclaim-the-wild", VariableItemSheet, {
    types: ["Variable"],
    makeDefault: true,
    label: "Variable Item"
  });

  preloadHandlebarsTemplates();

  // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper('concat', function () {
    var outStr = '';
    for (var arg in arguments) {
      if (typeof arguments[arg] != 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('toLowerCase', function (str) {
    return str.toLowerCase();
  });

  Handlebars.registerHelper('contains', function (str, text) {
    return str.includes(text);
  });

  Handlebars.registerHelper('debug', function (text, content) {
    return console.debug(text, content);
  });

  Handlebars.registerHelper('toSentenceCase', function (str) {
    return str.replace(/\w\S*/g, function (txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  });

  Handlebars.registerHelper('fetchEffect', function (str) {
    return new RegExp(/Effect<\/b>:([^<]+)/g).exec(str)[1] || "";
  });

  Handlebars.registerHelper('times', function (n, content) {
    let result = "";
    for (let i = 0; i < n; ++i) {
      result += content.fn(i);
    }
    return result;
  });
});
